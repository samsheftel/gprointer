from distutils.core import setup

setup(
    name='GProInter',
    version='0.0.1',
    packages=[''],
    url='',
    license='',
    author='Samuel Sheftel',
    author_email='sam.sheftel@american.edu',
    description='G Protein Interaction'
)
