__author__ = 'sam'

import os
import platform
MAC_PREFIX = os.path.join('User', 'sam')
VMWARE_PREFIX = os.path.join('mnt', 'hgfs')
IS_VMWARE_VM = platform.platform() == 'Linux-2.6.32-38-generic-x86_64-with-Ubuntu-10.04-lucid'


TEST_PATH = os.path.split(os.path.abspath(__file__))[0]
PDB_TEST_DATASET_DIR = "datasets"
TEST_OUTPUT_DIR = "output"
PDB_TEST_DATASET_DIR_PATH = os.path.join(TEST_PATH, PDB_TEST_DATASET_DIR) 
TEST_OUTPUT_DIR_PATH = os.path.join(TEST_PATH, TEST_OUTPUT_DIR) 


def pdb_path_getter(pdbname):
    """
    helper function for getting the path to a pdb for testing purposes
    :param pdbname: name of pdb
    :type pdbname: str
    :return: str
    """
    if not pdbname.endswith(".pdb"):
        pdbname += '.pdb'
    #testdir = os.path.split(os.path.abspath(__name__))[0]
    #testdir = os.path.split(os.path.abspath(__file__))[0]
    return os.path.join(TEST_PATH, PDB_TEST_DATASET_DIR, pdbname)

def allpdbs(dir):
    """list of pdb file paths under a given dir

    >>> allpdbs('/User/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/2RH1.pdb')
    """
    for dirpath, dirnames, filenames in os.walk(dir):
        for filename in filenames:
            if os.path.splitext(filename)[1] == '.pdb':
                yield os.path.join(dirpath, filename)
