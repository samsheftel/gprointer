__author__ = 'sam'

"""
"""
########################################################################################################################
from .context import gprointer
from . import pdb_path_getter
from gprointer import csuparse
import unittest
import pytest
import os
import platform
########################################################################################################################

class DictGetOrSetToAndReturnTest(unittest.TestCase):
    def test(self):
        d1 = {'name': 'Sam'}
        csuparse._dictGetOrSetToAndReturn(d1, 'friends', {})
        #self.assertDictEqual(d1, {'friends': {}, 'name': 'Sam'})
        assert d1 == {'friends': {}, 'name': 'Sam'}
        d2 = {'name': 'Sam'}
        csuparse._dictGetOrSetToAndReturn( csuparse._dictGetOrSetToAndReturn(d2, 'friends', {}), 'Amy', {})
        #self.assertDictEqual(d2, {'friends': {'Amy': {}}, 'name': 'Sam'})
        assert d2 == {'friends': {'Amy': {}}, 'name': 'Sam'}



def test_pdb_path_getter():
	if platform.platform() != 'Linux-2.6.32-38-generic-x86_64-with-Ubuntu-10.04-lucid':
		assert pdb_path_getter('1GZM.pdb') == '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/1GZM.pdb'
	else:
		assert pdb_path_getter('1GZM.pdb') == '/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/1GZM.pdb'    	





