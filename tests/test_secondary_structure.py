__author__ = 'sam'

"""
"""
########################################################################################################################
from .context import secondary_structure
from . import pdb_path_getter, allpdbs
import unittest
import pytest
import os
import platform
import random
########################################################################################################################
_2RH1_ = '2RH1.pdb'


def test_get_pross_data():
	pdb_path = pdb_path_getter( _2RH1_ )
	pross_data =  secondary_structure.get_pross_data(pdb_path)
	assert pross_data.get('A_VAL_33').classification == 'alphahelix'

def test_SecStructure_parse_res_id_str():
	assert secondary_structure.SecStructure._parse_res_id_str('A_VAL_33') == ('A', 'VAL', 33)

def test_get_alphahelix_ranges():
	pdb_path = pdb_path_getter( _2RH1_ )
	sec_structure = secondary_structure.SecStructure(pdb_path)
	assert sec_structure.get_alphahelix_ranges()[0] == ('A_GLU_30', 'A_LYS_60')

def test_get_alphahelixes():
	pdb_path = pdb_path_getter( _2RH1_ )
	sec_structure = secondary_structure.SecStructure(pdb_path)
	assert len(sec_structure.alphahelixes) == 10
	first_alpha_seq = sec_structure.alphahelixes[0]
	assert first_alpha_seq[0] == 'A_GLU_30' and first_alpha_seq[-1] == 'A_LYS_60'

def test_get_alphahelixes_primary_seqs():
	pdb_path = pdb_path_getter( _2RH1_ )
	sec_structure = secondary_structure.SecStructure(pdb_path)
	first_helix_primary_seq = sec_structure.get_alphahelixes_primary_seqs()[0]
	second_helix_primary_seq = sec_structure.get_alphahelixes_primary_seqs()[1]
	assert first_helix_primary_seq[:4] == ['E', 'V', 'W', 'V']
	assert second_helix_primary_seq[:4] == ['T', 'N', 'Y', 'F']

def test_get_alphahelix_pdb_lines():
	pdb_path = pdb_path_getter( _2RH1_ )
	sec_structure = secondary_structure.SecStructure(pdb_path)
	assert len(sec_structure.get_alphahelix_pdb_lines()) == 10
	assert sec_structure.get_alphahelix_pdb_lines()[0][0] == 'ATOM      6  N   GLU A  30     -53.394  -1.944  20.236  1.00 98.37           N  \n'
	assert sec_structure.get_alphahelix_pdb_lines()[0][-1] == 'ATOM    224  N   LYS A  60     -45.271  36.012  -2.184  1.00 76.98           N  \n'

def test_write_all_alphahelix_to_file():
	pdb_path = pdb_path_getter( _2RH1_ )
	sec_structure = secondary_structure.SecStructure(pdb_path)
	sec_structure.write_all_alphahelix_to_file('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/GProteinParse/PDBs/2RH1_all_alphas.pdb')
	assert os.path.exists('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/GProteinParse/PDBs/2RH1_all_alphas.pdb')

def test_write_all_alphahelix_to_file2():
	pdb_files = allpdbs('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/output')
	for pdb_file in pdb_files:
		sec_structure = secondary_structure.SecStructure(pdb_file)
		pdb_id = os.path.splitext(os.path.split(pdb_file)[1])[0]
		sec_structure.write_all_alphahelix_to_file('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/GProteinParse/PDBs/'+pdb_id +'_only alpha'+'.pdb')
	assert 0 == 1


