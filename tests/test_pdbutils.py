__author__ = 'sam'

"""
"""
########################################################################################################################
from .context import pdbutils, allpdbs
from . import TEST_OUTPUT_DIR_PATH
import unittest
import pytest
import os
import platform
########################################################################################################################

PDBS = """1F88 1HZX 1L9H 1U19 1GZM 2G87 2HPY 2I35 2I36 2I37 2PED 2J4Y 3C9Ld 3C9M
3CAP 3DQB 3OAX 2X72 3PQRf 3PXO 2ZIY 2Z73 3AYM 3AYN 2VT4 2Y00 2Y01 2Y02 2Y03 2Y04
2YCW 2YCX 2YCY 2YCZ 4AMI 4AMJ 2R4R 2R4S 2RH1 3D4S 3KJ6 3NY8 3NY9 3NYA 3PDS 3P0G
3SN6 3EML 2YDO 2YDV 3QAK 3PWH 3REY 3RFM 3VG9 3VGA 3UZA 3UZC"""

PDB_WITH_DBREF = """3NY8 3OAX 1GZM 2YCX 3PXO 2RH1 2I35 4AMJ 3PDS 2YCY 2Y02 2HPY
2J4Y 1U19 1F88 2PED 3NYA 2I36 2Y01 3D4S 2Y03 4AMI 3C9M 2Y00 2G87 2R4S 2Y04 1L9H
3P0G 2I37 2YCZ 2VT4 2X72 3KJ6 3EML 1HZX 3NY9 3DQB 3SN6 3QAK 2YCW 2R4R 3CAP"""

PDB_IDS = PDBS.replace('\n', ' ').split(' ')
PDB_WITH_DBREF_IDS = PDB_WITH_DBREF.replace('\n', ' ').split(' ')

# def test_download_pdb():
# 	for pdb_id in PDB_IDS:
# 		pdbutils.download_pdb(pdb_id, TEST_OUTPUT_DIR_PATH)
# 	saved_pdbs = [os.path.splitext(pdb_file)[0] for pdb_file in os.listdir(TEST_OUTPUT_DIR_PATH)]
# 	assert set(PDB_IDS) - set(saved_pdbs) == set()

def test_get_uniprot_id():
	 p0_ids = pdbutils.get_uniprot_id(PDB_WITH_DBREF_IDS[0])
	assert p0_ids == ['P07550']
