Hi Sam,

I am sending you a number of structured of the adrenergic receptors. I edited them in order to leave only the residues that are present in all the structures. Moreover, I gave the same orientation to all the structures, to facilitate the production of the figures. Please use these structures to perform the calculations that we mentioned when we met yesterday. Please focus primarily on 3POG an 2RH1 and analyze the other structures only if you have time - They shouldn't be that different from 2RH1.

Moreover, I further analyzed your preliminary data by visualizing on the structures of the receptors the residues with the highest centrality in 3POG and 2RH1 as well as those with the most pronounce change in centrality between 3P0G and 2RH1. The data make a lot of sense. The residues with the highest centrality are all in the core of the helical bundle. Those that gain centrality with the activation of the receptor are mostly located toward the extracellular end of the bundle, while those that lose centrality are mostly located toward the cytosolic end of the bundle. This is reasonable, because, with activation, the receptor shrinks on the extracellular side and opens up on the cytosolic side.

On another note, Tuesday afternoon does not work for me. Would you be able to come a little earlier than usual on Thursday so that we can talk longer?

Thank you,

SC