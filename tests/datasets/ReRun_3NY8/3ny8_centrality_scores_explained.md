#3NY8 Centrality Scores Explained


The Centrality score for PGE PHE-100 in 3NY8_L (with ligand)

    {
        "resnum": 1207, 
        "region_name": "", 
        "pymol": "/3NY8_eo//A/PGE`1207/", 
        "ballesteros": "", 
        "centrality_score": 0.17715768981181051, 
        "resname": "Pge"
    }

PGE only interacts with one residue - PHE-101
three non colvalent interaction makeup the contacts between PGE and PHE-101 (PGE'1207/C5 - PHE'101/CD1,  PGE'1207/C5 - PHE'101/CE1, PGE'1207/C4 - PHE`101/CE1)

	"/3NY8_eo//A/PGE`1207/C5": {
        "/3NY8_eo//A/PHE`101/CD1": {
            "Atom1Name": "C5", 
            "Residue1Name": "PGE", 
            "Residue1Number": 1207, 
            "Residue1Chain": "A", 
            "AccessibleSurface": 22.0, 
            "Distance": 3.8999999999999999, 
            "ContactSurface": 13.0, 
            "FreeAccessibleSurface": 136.80000000000001, 
            "Residue2Number": 101, 
            "Atom2Name": "CD1", 
            "Atom1Type": 8, 
            "Atom2Type": 5, 
            "Residue2Name": "PHE", 
            "Residue2Chain": "A"
        }, 
        "/3NY8_eo//A/PHE`101/CE1": {
            "Atom1Name": "C5", 
            "Residue1Name": "PGE", 
            "Residue1Number": 1207, 
            "Residue1Chain": "A", 
            "AccessibleSurface": 22.0, 
            "Distance": 4.0999999999999996, 
            "ContactSurface": 0.90000000000000002, 
            "FreeAccessibleSurface": 136.80000000000001, 
            "Residue2Number": 101, 
            "Atom2Name": "CE1", 
            "Atom1Type": 8, 
            "Atom2Type": 5, 
            "Residue2Name": "PHE", 
            "Residue2Chain": "A"
        }
    }, 
    "/3NY8_eo//A/PGE`1207/C4": {
        "/3NY8_eo//A/PHE`101/CE1": {
            "Atom1Name": "C4", 
            "Residue1Name": "PGE", 
            "Residue1Number": 1207, 
            "Residue1Chain": "A", 
            "AccessibleSurface": 31.899999999999999, 
            "Distance": 4.2999999999999998, 
            "ContactSurface": 1.6000000000000001, 
            "FreeAccessibleSurface": 136.80000000000001, 
            "Residue2Number": 101, 
            "Atom2Name": "CE1", 
            "Atom1Type": 8, 
            "Atom2Type": 5, 
            "Residue2Name": "PHE", 
            "Residue2Chain": "A"
        }
    }

The centrality of PHE-101 3NY8 (without ligand) is 0.20435762584522915

The centrality of PHE-101 in 3NY8_L (with ligand) is 0.20495495495495494

Centrality of PHE-101 is calulated by as follows:

$$C(x) = \frac{n-1}{\sum_{y \in \mathscr{U},y \neq x}{dist(x,y)}}$$

thus the equation for PHE-101 centrality with ligand considering it interacts with PGE will be 
$$C_L(x) = \frac{n}{\sum_{y \in \mathscr{U},y \neq x}{dist(x,y)}+1}$$

We know n for 3NY8 (without ligand) is 273 so PHE-101 centrality 
$$C(PHE101) = 0.20435762584522915 = \frac{273-1}{\sum_{y \in \mathscr{U},y \neq x}{dist(x,y)}}$$

so $$\sum_{y \in \mathscr{U},y \neq x} = 1331$$

calulating PHE-101 in 3NY8_L (with ligand) with this value 
$$C_L(PHE101) = \frac{273}{1331+1}$$

Which equals $$0.20495495495495494$$ thus afferming the centrality data 


Given that PGE only interacts with PHE-101