#!/usr/bin/env python

"""
the command line interface for calulating cnetrality data from csu data files

python runcentrality.py csu_data_file_path.json gProtien_index_file.gpi

will make two file in the same dir as the csu_data_file_path.json file
	1. pdb_id_centrality_data.csv
	2. pdb_id_centrality_data.json

"""

from pdbutils import loadjson, savetojson, savetocsv, readfile, makeNewFileNameFromOld
from closeness_centrality import CentralityCalculator, Centrality
from gproindexer import GProIndexer
from operator import attrgetter
import sys
import os

def main():
	arg_count = len(sys.argv)
	if arg_count < 3:
		raise ValueError("""not enough args - run syntax is:
			python runcentrality.py csu_data_file_path.json gProtien_index_file.gpi""")
	csu_data_file = os.path.abspath(sys.argv[1])
	residue_graph = loadjson(csu_data_file).get("ResidueData")
	centralityCalculator = CentralityCalculator( residue_graph )

	gprotien_index_file = os.path.abspath(sys.argv[2])
	gpProIndexer = GProIndexer( readfile(gprotien_index_file).strip() )

	Centrality_Data = centralityCalculator.getAllCenralityTup(gpProIndexer)
	Centrality_Data = sorted(Centrality_Data, key=attrgetter('resnum'), reverse=False)
	
	save_dir, csu_data_file_name = os.path.split(csu_data_file)
	pdb_id = csu_data_file_name[:csu_data_file_name.find('_csu_data.json')]
	save_file_name = pdb_id + '_centrality_data'
	save_file_path = os.path.join(save_dir, save_file_name)
	csv_save_file_path = save_file_path + '.csv'
	json_save_file_path = save_file_path + '.json'

	savetocsv(Centrality._fields, Centrality_Data, csv_save_file_path)
	print('saved file @ {save_file_path}'.format(save_file_path=csv_save_file_path))

	Centrality_Data_Dicts = [centrality._asdict() for centrality in Centrality_Data]
	savetojson(Centrality_Data_Dicts, json_save_file_path)
	print('saved file @ {save_file_path}'.format(save_file_path=json_save_file_path))

if __name__ == '__main__':
	main()
