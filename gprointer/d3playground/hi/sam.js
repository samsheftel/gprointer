(function(){
	console.log('hi');
	var width, height, svg, force, testGraph, color, link, node, texts;

	width = 960*3;
    
    height = 500*3;

    color = d3.scale.category20();

    svg = d3
        .select("body")
    	.append("svg")
    	.attr("width", width)
    	.attr("height", height);
    
    force = d3
        .layout.force()
    	.charge(-120*3)
    	.linkDistance(30*3)
    	.size([width, height]);

	testGraph = {
		nodes: [
			{"name": "node1", "group": 1},
			{"name": "node2", "group": 2}
		],
		links: [
			{"source":0, "target":1}
		]
	};


    force
        .nodes(testGraph.nodes)
        .links(testGraph.links)
        .start();

    link = svg
        .selectAll(".link")
        .data(testGraph.links)
        .enter().append("line")
        .attr("class", "link")
        .style("stroke-width", function(d) { return Math.sqrt(d.value); });

    node = svg
        .selectAll(".node")
        .data(testGraph.nodes)
        .enter().append("circle")
        .attr("class", "node")
        .attr("r", 5*3)
        .style("fill", function(d) { return color(d.group); })
        .call(force.drag);

    texts = svg.selectAll("text.label")
        .data(testGraph.nodes)
        .enter().append("text")
        .attr("class", "label")
        .attr("fill", "black")
        .text(function(d) {  return d.name;  });


    force.on("tick", function(){
        link.attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        node.attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });

        texts.attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
    });





}());