"""
this module contains the classes and fuction for the conversion
of a staderd residue index into the gpro indexing scheme


the gProtien indexing systom consists of three parts 
	1. region type
		(typename, typeindex)
	
	2. residue range
		(resindex, resindex)

	3. references residue for region
		(resindex, refres_numlabel)
		like:
		(79, 50) meaning that residue 79 is labeld as res 50

	if part 3 is spesifed the tehn index for a residue that falles within this
		region will be the {typeindex}.{pos_rel_to_references}


gProRegion = (type, num), resrange, refres


"""
import re
from collections import namedtuple

try:
	from pdbutils import parse_pymol_format
except:
	PYMOLSELECTIONPATTERN = re.compile('/([\w\d]+)//(\w)/((\w+)`(\d+))(/(\w+)?)?')
	def parse_pymol_format(pymol_format_str):
		matchobj = PYMOLSELECTIONPATTERN.match(pymol_format_str.strip())
		struct, chain, res, resname, resnum, slash, atomname = matchobj.groups()
		resnum = int(resnum) if resnum.isdigit() else resnum
		return struct, chain, resname, resnum, atomname

GProRegion = namedtuple('GProRegion', ["region_name", "region_num", "first_res", "last_res", "reference_res", "designation_num", "lable_template"])

reg_name_pattern = re.compile('(?P<name>[a-zA-Z]+)(?P<num>\d+)?')
reg_range_pattern = re.compile('[ \t]*?(?P<first_res>[0-9]+)?[ \t]*?(-){1}([ \t])*(?P<last_res>[0-9]+)?')
def region_name_parse(regname):
	"""takes a region name raw string and breaks it into its name and index
		parts

	>>> region_name_parse('TM1')
	('TM', 1)
	"""
	matchdict = reg_name_pattern.match(regname).groupdict()
	return matchdict['name'], matchdict['num']

def residue_range_parse(resrange):
	"""parse out the first and last reisidue which make up the region bondry

	>>> residue_range_parse('10-44')
	(10, 44)
	>>> residue_range_parse('-44')
	(0, 44)
	>>> residue_range_parse('12-')
	(12, float('inf'))
	"""
	firstres, lastres = resrange.split('-')
	firstres = int(firstres) if firstres.isdigit() else 0
	lastres = int(lastres) if lastres.isdigit() else 1000000
	return firstres, lastres

def reference_residue_parse(refres_config):
	"""parses refres_config into its compontes with defauls

	:param refres_config: string of format
		'global_res_num designation_num lable_template'

	>>> reference_residue_parse("36 50 '{region_num}.{reference_offset}'")
	(36, 50, '{region_num}.{reference_offset}')

	>>> reference_residue_parse("36")
	(36, 50, '{region_num}.{reference_offset}')

	"""
	res_num, designation_num, lable_template = 0, 0, ''
	num_of_spaces = refres_config.count(' ')
	if num_of_spaces == 2:
		res_num, designation_num, lable_template = refres_config.split(' ')
	elif num_of_spaces == 0 and refres_config.isdigit():
		res_num, designation_num, lable_template = int(refres_config), 50, '{region_num}.{reference_offset}'
	return int(res_num), int(designation_num), lable_template


def section_spliter(linestr):
	eq_or_colon_index = linestr.find('=')
	if eq_or_colon_index == -1: eq_or_colon_index = linestr.find(':')
	
	region_str = linestr[:eq_or_colon_index].strip()
	linestr = linestr[eq_or_colon_index+1:]
	range_match = reg_range_pattern.match(linestr)
	range_match_dict = range_match.groupdict()
	reference_str = linestr[range_match.span()[1]+1:].strip()

	region_name, region_num = region_name_parse(region_str)
	first_res = int(range_match_dict.get('first_res', 0))
	last_res = int(range_match_dict.get('last_res', 1000000))
	reference_res, designation_num, lable_template = reference_residue_parse(reference_str)

	return GProRegion(region_name, region_num, first_res, last_res, reference_res, designation_num, lable_template)


class GProIndexer(object):
	"""
	class that encapilates teh logic of converting a residue number into a 
	gpro index based on the confiugred regions

	>>> gpProIndexer = GProIndexer('''Nt = 1 - 28
			TM1 = 29 - 60 50
			IL1 =  61 - 66
			TM2 = 67 - 96 79
			EL1 = 97 - 101
			TM3 = 102 - 136 131
			IL2 = 137 - 146
			TM4 = 147 - 170 158
			EL2 = 171 - 196
			TM5 = 197 - 230 211
			IL3 = 231 - 266
			TM6 = 267 - 298 288
			EL3 = 299 - 304
			TM7 = 305 - 328 323
			H8 = 329 - 341
			Ct = 342 - 413''')
	>>> len(gpProIndexer)
	16

	"""
	def __init__(self, region_spec_lines):
		self.regions = [section_spliter(spec_line.strip()) for spec_line in region_spec_lines.splitlines() if spec_line.strip()]

	def __iter__(self):
		return iter(self.regions)
	def __len__(self):
		return len(self.regions)

	def getregion(self, res_num):
		"""returns the gproregion that the residue with number res_num is in"""
		for gProRegion in self:
			if res_num >= gProRegion.first_res and res_num <= gProRegion.last_res:
				return gProRegion
		return None

	def translate_from_pymol_format(self, pymol_format_str):
		struct, chain, resname, resnum, atomname = parse_pymol_format(pymol_format_str)
		return self.translate(resnum)


	def translate(self, res_num):
		"""takes a residue number and traslate it into its gprotein index

		>>> gpProIndexer = GProIndexer('TM2 = 67 - 96 79')
		>>> gpProIndexer.translate(76)
		('TM2', '2.47')

		"""
		region_name, ballesteros = '', ''
		region = self.getregion(res_num)
		if region:
			region_name = region.region_name
			if region.lable_template and region.designation_num and region.reference_res:
				reference_offset = region.designation_num + (res_num - region.reference_res)
				ballesteros = region.lable_template.format(reference_offset=reference_offset, **region._asdict())
		return region_name, ballesteros




