
"""
when runing prorint on multiple pdb of the same type of respotr
	the residue indexes will be the same so any missing res indexs in one
	pdb must be removed from the other this scrip does that

"""

from Bio.PDB.PDBParser import PDBParser
import os
from collections import namedtuple

def namedtuple_decor(func):
	"""decorator for creating a named tuple

	>>> @namedtuple_decor
	... def Person(first_name, last_name, age): pass
	>>> Person("Sam", "Sheftel", 25)
	Person(first_name='Sam', last_name='Sheftel', age=25)
	"""
	return namedtuple(func.__name__, func.__code__.co_varnames)

@namedtuple_decor
def PdbLines(file_path, pdb_id, lines): pass


def getPdbPathAndProteinName(pdbFileObjOrPath):
    """
    takes either a pdb file path as a string or a file obj, returns the path of the pdb
        and the name of the Structure
    """
    if isinstance(pdbFileObjOrPath, file):
        pdbFileObjOrPath = pdbFileObjOrPath.name
    absolutePdbFilePath = os.path.abspath(pdbFileObjOrPath)
    pdbFileDir, pdbFileName = os.path.split(absolutePdbFilePath)
    proteinName, pdbFileExtention = os.path.splitext(pdbFileName)
    return (absolutePdbFilePath, proteinName)
def parserPdb(pdbFileObjOrPath):
    """
    returns the Biopython pdb object for the pdb file | file obj passed in
    """
    parser = PDBParser()
    absolutePdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
    return parser.get_structure(proteinName, absolutePdbFilePath)

def read_file_lines(file_path):
	"""returns a iterator for the file lines which closes th iter when done 
	reading file lines
	"""
	with open(file_path, 'r') as f:
		for line in f:
			yield line

@namedtuple_decor
def PdbLines(file_path, pdb_id, lines): pass

def getPdbLines(pdbFileObjOrPath):
	"""
	returns a PdbLines Named Tuple, keys are (file_path, pdb_id, lines)
	lines will be a iter
	"""
	absolutePdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
	pdb_lines = read_file_lines(absolutePdbFilePath)
	return PdbLines(absolutePdbFilePath, proteinName, pdb_lines)


def getAtomIndexFromLine(pdb_line):
	"""returns the atom index and bool indicating weather the atom is a heteroatom 

	>>> getAtomIndexFromLine('ATOM   3542  CD1 LEU A 342     -54.415  35.913  12.368  1.00 82.95           C  ')
	(342, False)
	"""
	atom_index, is_hetero = 0, False
	if pdb_line[:6] == 'HETATM':
		atom_index, is_hetero = int(pdb_line[22:26]), True
	if pdb_line[:4] == 'ATOM' or pdb_line[:3] == 'TER':
		atom_index = int(pdb_line[22:26])
	return atom_index, is_hetero




def getPdbLinesWithAtomIndex(pdb_lines):
	"""given a pdb lines list or lines iterator this will return a new line iter
	which returns a tuple with first value being the line and the 2nd the atom 
	number. if the line is a not a atom spec line then 2nd item will be None
	"""
	for pdb_line in pdb_lines:
		if pdb_line[:4] == 'ATOM' or pdb_line[:3] == 'TER':
			pdb_line = pdb_line.split()
			res_index = int(pdb_line[5])
			yield pdb_lines, res_index
		else:
			yield pdb_lines, None


def getResObjIterForPdb(pdbFileObjOrPath):
	"""ASSUMES ONLY ONE CHAIN!
	returns a iterator of the residueObj from BioPython
	"""
	pdb = parserPdb(pdbFileObjOrPath)
	return list(pdb.get_chains())[0].get_residues()

def getAllResIndexsWithHeteroNameInPdb(pdbFileObjOrPath):
	"""takes a pdb and returns a iterator over the residue indexs"""
	residues = getResObjIterForPdb(pdbFileObjOrPath)
	for res in residues:
		yield res.get_full_id()[3][1], res.get_full_id()[3][0]

def getAllResIndexsInPdb(pdbFileObjOrPath):
	"""takes a pdb and returns a iterator over the residue indexs"""
	for res_index, hetero_name in getAllResIndexsWithHeteroNameInPdb(pdbFileObjOrPath):
		yield res_index

def getIndexOfHeteroAtomsInPdb(pdbFileObjOrPath):
	for res_index, hetero_name in getAllResIndexsWithHeteroNameInPdb(pdbFileObjOrPath):
		if hetero_name.strip():
			yield res_index

def makeNewFileNameFromOld(file_path, append_str, new_extension=None):
	"""takes a file_path with name and modifyes the pre extetion part

	>>> file_path = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/utils.py"
	>>> makeNewFileNameFromOld(file_path, "jim_jim")
	'/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/utils_jim_jim.py'
	"""
	file_path = os.path.abspath(file_path)
	append_str = append_str if append_str.startswith('_') else '_'+ append_str
	if not os.path.exists(file_path) or not os.path.isfile(file_path):
		raise NameError("file path given not exist")
	dir_path, file_name = os.path.split(file_path)
	name, extension = os.path.splitext(file_name)
	extension = new_extension or extension
	extension = extension if extension[0] == '.' else '.' + extension
	new_file_name = name + append_str + extension
	new_path = os.path.join(dir_path, new_file_name)
	return new_path


def write_lines(file_name, lines):
	file_name = os.path.abspath(file_name)
	with open(file_name, 'w') as f:
		for line in lines:
			f.write(line)


def normalize_pdb_indexes(*pdb_files, **kwargs):
	"""takes list of pdbs and removes lines that arnt same in both if there
	not hetero atoms

	kwarg:
		"include_hetero" determins if hetero atoms should be included
		"file_sufix"
	"""
	include_hetero = kwargs.get('include_hetero', True)
	file_sufix = kwargs.get("file_sufix", "normalized")
	pdb_files = [os.path.abspath(pdb_file) for pdb_file in pdb_files]
	res_index_sets = [set(getAllResIndexsInPdb(pdb_file)) for pdb_file in pdb_files]
	res_index_intersection = reduce(set.intersection, res_index_sets)
	for pdb_file_path in pdb_files:
		pdbLinesTup = getPdbLines(pdb_file_path)
		new_file_name = makeNewFileNameFromOld(pdbLinesTup.file_path, file_sufix)
		with open(new_file_name, 'w') as f:
			for line in pdbLinesTup.lines:
				atom_index, is_hetero = getAtomIndexFromLine(line)
				if is_hetero and not include_hetero: continue
				elif atom_index and atom_index not in res_index_intersection and not is_hetero: continue
				f.write(line)



		