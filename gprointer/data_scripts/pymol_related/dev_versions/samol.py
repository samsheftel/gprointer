"""
My Set Of Simple PyMol Utility Functions
Samol
Path to pymol python /Applications/PyMOLX11Hybrid.app/pymol/ext/lib/python2.5/

path to pymol modules: /Applications/PyMOLX11Hybrid.app/pymol/modules/pymol

WAY TO RUN execfile('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/pymolgui/samol.py')
"""
import sys
sys.path.insert(0, '/Applications/PyMOLX11Hybrid.app/pymol/modules/pymol')
try:
    import cmd
	import _cmd
	import util
except ImportError:
	cmd = MockPymolCmd()
	util = MockPymolUtil()

class MockPymolCmd(object):
	def __init__(self):
		pass
	def color(self, clr, selstr):
		cmdstr = 'cmd.color(%s, %s)' % (clr, selstr)
		print(cmdstr)
		return cmdstr
	def show(self, shape, selstr):
		cmdstr = 'cmd.show(%s, %s)' % (shape, selstr)
		return cmdstr
	def select(self, name, selstr):
		cmdstr = 'cmd.select(%s, %s)' % (name, selstr)
		return cmdstr
	def hide(self, shapes, selstr):
		cmdstr = 'cmd.hide(%s, %s)' % (shapes, selstr)
		return cmdstr
class MockPymolUtil(object):
	def __init__(self):
		pass
	def util.cba(colornum, selstr, context):
		cmdstr = 'util.cba(%s, %s, %s)' % (colornum, selstr, context)
		return cmdstr


#
def forceList(arg):
	if not isinstance(arg, list):
		arg = [arg]
	return arg

def resetColor(selstr):
	util.cba(33, selstr, _self=cmd)

def resetShape(selstr):
	cmd.hide("everything", selstr)
	cmd.show("lines", selstr)

def selResNums(resnumlist):
	resnumlist = forceList(resnumlist)
	reses = '+'.join([str(resnum) for resnum in resnumlist])
	selstr = "i. "+ reses
	return selstr

def selElmTypeInRes(resnumlist, elmtypelist):
	elmtypelist = forceList(elmtypelist)
	emls = '+'.join([str(emltype) for emltype in elmtypelist])
	selstr = 'e. ' + emltype + ' and ' + elResNums(selResNums) + selResNums(selResNums)
	return selstr

def mkgreen(selstr):
	cmd.color("green", selstr)

def mkred(selstr):
	cmd.color("red", selstr)

def mkspheres(selstr):
	cmd.show("spheres", selstr)

def mklines(selstr):
	cmd.show("lines", selstr)

def slc(selstr, name):
	cmd.select(name, selstr)
















