"""
pymol_centrality_gui_manager
"""
from __future__ import with_statement
from Tkinter import *
import tkFileDialog
import os

import pymol_centrality_classes
import centrality_windows

LOCALSTARTPATH = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/CostanziEmail"

# class PyMolMixin(object):
# 	def groupSel(self, listOfResNum, groupname):
# 		"""selects a group of residues in to a single selection with a optional name """
# 		pass
# 	def setResColor(self, resnum, color):
# 		pass
# 	def setResStyle(self, resnum, style):
# 		pass
# 	def resetResColor(self, resnum):
# 		pass
# 	def resetResStyle(self, resnum):
# 		pass
# 	def resetAllColor(self, resnums):
# 		util.cba(33,"sele",_self=cmd)
# 		reses = '+'.join([str(resnum) for resnum in resnums])
# 		oxigen_sel = "e. o and %s" % reses
# 		nitrogen_sel = "e. n and %s" % reses
# 		all_reses_sel = "i. %s" % reses
# 		cmd.hide("everything", all_reses_sel)# clear res show shape
# 		cmd.show("lines", all_reses_sel)# show res with line shape
# 		cmd.color("green", all_reses_sel)# rest color to Green
# 		cmd.color("red", oxigen_sel)# set oxigen to red
# 		cmd.color("green", nitrogen_sel)# set nitrogen to red

# 		cmd.



class CentralityViewerManagerSimple():
	def __init__(self):
		self.sourceCentralityCsv = self.askCsvName()
		self.centList = pymol_centrality_classes.CentList.fromCsv(self.sourceCentralityCsv)
		self._rootwin = centrality_windows.getRootTkWin()
		self.guiWin = centrality_windows.SingleScrollerWindow(
			self._rootwin,
			self.centList.minCent().centrality_score,
			self.centList.maxCent().centrality_score,
			self.onApplyBtnCallback)

	def askCsvName(self, title="Select A Centrality Data CSV file"):
		startdir = LOCALSTARTPATH if os.path.exists(LOCALSTARTPATH) else os.path.expanduser('~') 
		return tkFileDialog.askopenfilename(
			filetypes=[('.csv', '.csv')],
			initialdir=startdir,
			multiple=False,
			title=title
			)
	def getCentListFromCsv(self, fpath):
		"""returns a CentList instance """
		return pymol_centrality_classes.CentList.fromCsv(fpath)

	def onApplyBtnCallback(self):
		print(self.guiWin.VAL)
		print(self.getCurrentHighlightedSet())
		print(self.resNumWithCentralityBigerThenScale())

	def getCurrentHighlightedSet(self):
		return set([int(resnum) for resnum in  self.guiWin.getDispText().strip().split(', ') if resnum])

	def resCentWithCentralityBigerThenScale(self):
		return self.centList.centBigerThen(self.guiWin.getScale())
	def resNumWithCentralityBigerThenScale(self):
		return [rescent.resnum for rescent in self.resCentWithCentralityBigerThenScale()]


if __name__ == '__main__':
	CVMS = CentralityViewerManagerSimple()
	CVMS._rootwin.mainloop()