
from Tkinter import *
def blankcb():
	return 'hi'
class PyMolCentralityGui(Frame):
	valid_colors = ['yellow', 'red', 'green', 'brown', 'gold', 'gray', 'blue']
	scale_factor = 1000
	def __init__(self, parent, low, high, apply_cb=None):
		"""
		width=500, height=400
		"""
		if apply_cb == None:
			apply_cb = blankcb
		Frame.__init__(self, parent)
		if low < 1:
			low = low * self.scale_factor
		if high < 1:
			high = high * self.scale_factor
		self.Scl_low = low
		self.Scl_high = high
		self.Scl = Scale(root, from_=low, to=high, label='cut off', orient=HORIZONTAL)
		self.ClrLbl = Label(self, text="Color")
		self.ClrVar = StringVar()
		self.ClrInput = Entry(self, textvariable=self.ClrVar)
		self.Btn = Button(self, text='Apply', command=apply_cb)
		self.Txt = Text(self,height=10,width=70,background='white')
		self.Txt.tag_configure('bold_italics', font=('Verdana', 7, 'bold', 'italic'))

		self.Scl.grid(row=0,columnspan=2, sticky=W+E)
		self.ClrLbl.grid(row=1)
		self.ClrInput.grid(row=1, column=1)
		self.Btn.grid(row=3, columnspan=2, sticky=W+E)
		self.Txt.grid(row=4,column=0, columnspan=3)

		self.grid()

	def getDispText(self):
		return self.Txt.get('1.0', 'end')
	def appendText(self, text):
		self.Txt.set(END, text)
	def setText(self, text):
		self.clearText()
		self.Txt.set(END, text)
	def clearText(self):
		self.Txt.delete('1.0', 'end')

	def getScale(self):
		cur_sacle_val = float(self.Scl.get())
		if cur_sacle_val > 1:
			cur_sacle_val = cur_sacle_val / self.scale_factor
		return cur_sacle_val
	
	def setColor(self, color):
		self.ClrVar.set(color)

	def getColor(self):
		cur_color = self.ClrInput.get().strip()
		if cur_color not in self.valid_colors:
			cur_color = 'red'
			self.setColor(cur_color)
		return cur_color

	@property
	def VAL(self):
		return (self.getScale(), self.getColor())







if __name__ == '__main__':
	root = Tk()
	CentGui = PyMolCentralityGui(root, 0.151, 0.332)
