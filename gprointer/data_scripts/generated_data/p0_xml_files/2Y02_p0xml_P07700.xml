<?xml version='1.0' encoding='UTF-8'?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/support/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-04-01" modified="2013-01-09" version="94">
<accession>P07700</accession>
<name>ADRB1_MELGA</name>
<protein>
<recommendedName>
<fullName>Beta-1 adrenergic receptor</fullName>
</recommendedName>
<alternativeName>
<fullName>Beta-1 adrenoreceptor</fullName>
<shortName>Beta-1 adrenoceptor</shortName>
<shortName>Beta-T</shortName>
</alternativeName>
</protein>
<gene>
<name type="primary">ADRB1</name>
</gene>
<organism>
<name type="scientific">Meleagris gallopavo</name>
<name type="common">Common turkey</name>
<dbReference type="NCBI Taxonomy" id="9103"/>
<lineage>
<taxon>Eukaryota</taxon>
<taxon>Metazoa</taxon>
<taxon>Chordata</taxon>
<taxon>Craniata</taxon>
<taxon>Vertebrata</taxon>
<taxon>Euteleostomi</taxon>
<taxon>Archosauria</taxon>
<taxon>Dinosauria</taxon>
<taxon>Saurischia</taxon>
<taxon>Theropoda</taxon>
<taxon>Coelurosauria</taxon>
<taxon>Aves</taxon>
<taxon>Neognathae</taxon>
<taxon>Galliformes</taxon>
<taxon>Phasianidae</taxon>
<taxon>Meleagridinae</taxon>
<taxon>Meleagris</taxon>
</lineage>
</organism>
<reference key="1">
<citation type="journal article" date="1986" name="Proc. Natl. Acad. Sci. U.S.A." volume="83" first="6795" last="6799">
<title>The avian beta-adrenergic receptor: primary structure and membrane topology.</title>
<authorList>
<person name="Yarden Y."/>
<person name="Rodriguez H."/>
<person name="Wong S.K.-F."/>
<person name="Brandt D.R."/>
<person name="May D.C."/>
<person name="Burnier J."/>
<person name="Harkins R.N."/>
<person name="Chen E.Y."/>
<person name="Ramachandran J."/>
<person name="Ullrich A."/>
<person name="Ross E.M."/>
</authorList>
<dbReference type="MEDLINE" id="86313664"/>
<dbReference type="PubMed" id="3018746"/>
<dbReference type="DOI" id="10.1073/pnas.83.18.6795"/>
</citation>
<scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
</reference>
<reference key="2">
<citation type="journal article" date="1995" name="FEBS Lett." volume="358" first="133" last="136">
<title>NMR and circular dichroism studies of synthetic peptides derived from the third intracellular loop of the beta-adrenoceptor.</title>
<authorList>
<person name="Jung H."/>
<person name="Windhaber R."/>
<person name="Palm D."/>
<person name="Schnackerz K.D."/>
</authorList>
<dbReference type="MEDLINE" id="95129696"/>
<dbReference type="PubMed" id="7828722"/>
<dbReference type="DOI" id="10.1016/0014-5793(94)01409-T"/>
</citation>
<scope>STRUCTURE BY NMR OF 345-359</scope>
</reference>
<reference key="3">
<citation type="journal article" date="2008" name="Nature" volume="454" first="486" last="491">
<title>Structure of a beta1-adrenergic G-protein-coupled receptor.</title>
<authorList>
<person name="Warne T."/>
<person name="Serrano-Vega M.J."/>
<person name="Baker J.G."/>
<person name="Moukhametzianov R."/>
<person name="Edwards P.C."/>
<person name="Henderson R."/>
<person name="Leslie A.G.W."/>
<person name="Tate C.G."/>
<person name="Schertler G.F.X."/>
</authorList>
<dbReference type="PubMed" id="18594507"/>
<dbReference type="DOI" id="10.1038/nature07101"/>
</citation>
<scope>X-RAY CRYSTALLOGRAPHY (2.7 ANGSTROMS) OF 33-367 IN COMPLEX WITH THE ANTAGONIST CYANOPINDOLOL</scope>
<scope>DISULFIDE BONDS</scope>
<scope>FUNCTION</scope>
<scope>TOPOLOGY</scope>
</reference>
<comment type="function">
<text evidence="1">Beta-adrenergic receptors mediate the catecholamine-induced activation of adenylate cyclase through the action of G proteins. This receptor binds epinephrine and norepinephrine with approximately equal affinity.</text>
</comment>
<comment type="subcellular location">
<subcellularLocation>
<location>Cell membrane</location>
<topology>Multi-pass membrane protein</topology>
</subcellularLocation>
</comment>
<comment type="PTM">
<text>Homologous desensitization of the receptor is mediated by its phosphorylation by beta-adrenergic receptor kinase.</text>
</comment>
<comment type="similarity">
<text>Belongs to the G-protein coupled receptor 1 family. Adrenergic receptor subfamily. ADRB1 sub-subfamily.</text>
</comment>
<dbReference type="EMBL" id="M14379">
<property type="protein sequence ID" value="AAA49627.1"/>
<property type="molecule type" value="mRNA"/>
</dbReference>
<dbReference type="PIR" id="A25896">
<property type="entry name" value="A25896"/>
</dbReference>
<dbReference type="UniGene" id="Mga.4462"/>
<dbReference type="PDB" id="1DEP">
<property type="method" value="NMR"/>
<property type="chains" value="A=345-359"/>
</dbReference>
<dbReference type="PDB" id="2VT4">
<property type="method" value="X-ray"/>
<property type="resolution" value="2.70"/>
<property type="chains" value="A/B/C/D=33-367"/>
</dbReference>
<dbReference type="PDB" id="2Y00">
<property type="method" value="X-ray"/>
<property type="resolution" value="2.50"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2Y01">
<property type="method" value="X-ray"/>
<property type="resolution" value="2.60"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2Y02">
<property type="method" value="X-ray"/>
<property type="resolution" value="2.60"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2Y03">
<property type="method" value="X-ray"/>
<property type="resolution" value="2.85"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2Y04">
<property type="method" value="X-ray"/>
<property type="resolution" value="3.05"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2YCW">
<property type="method" value="X-ray"/>
<property type="resolution" value="3.00"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2YCX">
<property type="method" value="X-ray"/>
<property type="resolution" value="3.25"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2YCY">
<property type="method" value="X-ray"/>
<property type="resolution" value="3.15"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="2YCZ">
<property type="method" value="X-ray"/>
<property type="resolution" value="3.65"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="4AMI">
<property type="method" value="X-ray"/>
<property type="resolution" value="3.20"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDB" id="4AMJ">
<property type="method" value="X-ray"/>
<property type="resolution" value="2.30"/>
<property type="chains" value="A/B=33-368"/>
</dbReference>
<dbReference type="PDBsum" id="1DEP"/>
<dbReference type="PDBsum" id="2VT4"/>
<dbReference type="PDBsum" id="2Y00"/>
<dbReference type="PDBsum" id="2Y01"/>
<dbReference type="PDBsum" id="2Y02"/>
<dbReference type="PDBsum" id="2Y03"/>
<dbReference type="PDBsum" id="2Y04"/>
<dbReference type="PDBsum" id="2YCW"/>
<dbReference type="PDBsum" id="2YCX"/>
<dbReference type="PDBsum" id="2YCY"/>
<dbReference type="PDBsum" id="2YCZ"/>
<dbReference type="PDBsum" id="4AMI"/>
<dbReference type="PDBsum" id="4AMJ"/>
<dbReference type="ProteinModelPortal" id="P07700"/>
<dbReference type="SMR" id="P07700">
<property type="residue range" value="39-359"/>
</dbReference>
<dbReference type="HOVERGEN" id="HBG106962"/>
<dbReference type="EvolutionaryTrace" id="P07700"/>
<dbReference type="GO" id="GO:0016021">
<property type="term" value="C:integral to membrane"/>
<property type="evidence" value="IEA:UniProtKB-KW"/>
</dbReference>
<dbReference type="GO" id="GO:0005886">
<property type="term" value="C:plasma membrane"/>
<property type="evidence" value="IEA:UniProtKB-SubCell"/>
</dbReference>
<dbReference type="GO" id="GO:0004940">
<property type="term" value="F:beta1-adrenergic receptor activity"/>
<property type="evidence" value="IEA:InterPro"/>
</dbReference>
<dbReference type="InterPro" id="IPR000507">
<property type="entry name" value="Adrgc_rcpt_B1"/>
</dbReference>
<dbReference type="InterPro" id="IPR002233">
<property type="entry name" value="Adrnrgc_rcpt"/>
</dbReference>
<dbReference type="InterPro" id="IPR000276">
<property type="entry name" value="GPCR_Rhodpsn"/>
</dbReference>
<dbReference type="InterPro" id="IPR017452">
<property type="entry name" value="GPCR_Rhodpsn_7TM"/>
</dbReference>
<dbReference type="Pfam" id="PF00001">
<property type="entry name" value="7tm_1"/>
<property type="match status" value="1"/>
</dbReference>
<dbReference type="PRINTS" id="PR01103">
<property type="entry name" value="ADRENERGICR"/>
</dbReference>
<dbReference type="PRINTS" id="PR00561">
<property type="entry name" value="ADRENRGCB1AR"/>
</dbReference>
<dbReference type="PRINTS" id="PR00237">
<property type="entry name" value="GPCRRHODOPSN"/>
</dbReference>
<dbReference type="PROSITE" id="PS00237">
<property type="entry name" value="G_PROTEIN_RECEP_F1_1"/>
<property type="match status" value="1"/>
</dbReference>
<dbReference type="PROSITE" id="PS50262">
<property type="entry name" value="G_PROTEIN_RECEP_F1_2"/>
<property type="match status" value="1"/>
</dbReference>
<proteinExistence type="evidence at protein level"/>
<keyword id="KW-0002">3D-structure</keyword>
<keyword id="KW-1003">Cell membrane</keyword>
<keyword id="KW-0181">Complete proteome</keyword>
<keyword id="KW-1015">Disulfide bond</keyword>
<keyword id="KW-0297">G-protein coupled receptor</keyword>
<keyword id="KW-0325">Glycoprotein</keyword>
<keyword id="KW-0449">Lipoprotein</keyword>
<keyword id="KW-0472">Membrane</keyword>
<keyword id="KW-0564">Palmitate</keyword>
<keyword id="KW-0597">Phosphoprotein</keyword>
<keyword id="KW-0675">Receptor</keyword>
<keyword id="KW-1185">Reference proteome</keyword>
<keyword id="KW-0807">Transducer</keyword>
<keyword id="KW-0812">Transmembrane</keyword>
<keyword id="KW-1133">Transmembrane helix</keyword>
<feature type="chain" description="Beta-1 adrenergic receptor" id="PRO_0000069120">
<location>
<begin position="1"/>
<end position="483"/>
</location>
</feature>
<feature type="topological domain" description="Extracellular" evidence="1">
<location>
<begin position="1"/>
<end position="38"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=1">
<location>
<begin position="39"/>
<end position="67"/>
</location>
</feature>
<feature type="topological domain" description="Cytoplasmic" evidence="1">
<location>
<begin position="68"/>
<end position="76"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=2">
<location>
<begin position="77"/>
<end position="103"/>
</location>
</feature>
<feature type="topological domain" description="Extracellular" evidence="1">
<location>
<begin position="104"/>
<end position="115"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=3">
<location>
<begin position="116"/>
<end position="137"/>
</location>
</feature>
<feature type="topological domain" description="Cytoplasmic" evidence="1">
<location>
<begin position="138"/>
<end position="155"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=4">
<location>
<begin position="156"/>
<end position="179"/>
</location>
</feature>
<feature type="topological domain" description="Extracellular" evidence="1">
<location>
<begin position="180"/>
<end position="205"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=5">
<location>
<begin position="206"/>
<end position="231"/>
</location>
</feature>
<feature type="topological domain" description="Cytoplasmic" evidence="1">
<location>
<begin position="232"/>
<end position="285"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=6">
<location>
<begin position="286"/>
<end position="315"/>
</location>
</feature>
<feature type="topological domain" description="Extracellular" evidence="1">
<location>
<begin position="316"/>
<end position="320"/>
</location>
</feature>
<feature type="transmembrane region" description="Helical; Name=7">
<location>
<begin position="321"/>
<end position="343"/>
</location>
</feature>
<feature type="topological domain" description="Cytoplasmic" evidence="1">
<location>
<begin position="344"/>
<end position="483"/>
</location>
</feature>
<feature type="region of interest" description="Agonist and antagonist binding">
<location>
<begin position="201"/>
<end position="215"/>
</location>
</feature>
<feature type="region of interest" description="Agonist and antagonist binding">
<location>
<begin position="303"/>
<end position="310"/>
</location>
</feature>
<feature type="region of interest" description="Agonist and antagonist binding">
<location>
<begin position="329"/>
<end position="333"/>
</location>
</feature>
<feature type="binding site" description="Agonist or antagonist">
<location>
<position position="121"/>
</location>
</feature>
<feature type="binding site" description="Agonist or antagonist" status="by similarity">
<location>
<position position="126"/>
</location>
</feature>
<feature type="lipid moiety-binding region" description="S-palmitoyl cysteine" status="by similarity">
<location>
<position position="358"/>
</location>
</feature>
<feature type="glycosylation site" description="N-linked (GlcNAc...)" status="probable">
<location>
<position position="14"/>
</location>
</feature>
<feature type="disulfide bond" evidence="1">
<location>
<begin position="114"/>
<end position="199"/>
</location>
</feature>
<feature type="disulfide bond" evidence="1">
<location>
<begin position="192"/>
<end position="198"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="33"/>
<end position="68"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="70"/>
<end position="72"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="75"/>
<end position="92"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="94"/>
<end position="104"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="111"/>
<end position="144"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="146"/>
<end position="152"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="155"/>
<end position="178"/>
</location>
</feature>
<feature type="turn">
<location>
<begin position="179"/>
<end position="182"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="187"/>
<end position="194"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="205"/>
<end position="215"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="217"/>
<end position="235"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="279"/>
<end position="315"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="317"/>
<end position="319"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="322"/>
<end position="334"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="335"/>
<end position="337"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="339"/>
<end position="342"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="343"/>
<end position="345"/>
</location>
</feature>
<feature type="helix">
<location>
<begin position="347"/>
<end position="357"/>
</location>
</feature>
<evidence key="1" type="ECO:0000006">
<source>
<dbReference type="PubMed" id="18594507"/>
</source>
</evidence>
<sequence length="483" mass="54078" checksum="B11A7E71F6CCE3E4" modified="1988-04-01" version="1">
MGDGWLPPDCGPHNRSGGGGATAAPTGSRQVSAELLSQQWEAGMSLLMALVVLLIVAGNV
LVIAAIGRTQRLQTLTNLFITSLACADLVMGLLVVPFGATLVVRGTWLWGSFLCECWTSL
DVLCVTASIETLCVIAIDRYLAITSPFRYQSLMTRARAKVIICTVWAISALVSFLPIMMH
WWRDEDPQALKCYQDPGCCDFVTNRAYAIASSIISFYIPLLIMIFVYLRVYREAKEQIRK
IDRCEGRFYGSQEQPQPPPLPQHQPILGNGRASKRKTSRVMAMREHKALKTLGIIMGVFT
LCWLPFFLVNIVNVFNRDLVPDWLFVFFNWLGYANSAFNPIIYCRSPDFRKAFKRLLCFP
RKADRRLHAGGQPAPLPGGFISTLGSPEHSPGGTWSDCNGGTRGGSESSLEERHSKTSRS
ESKMEREKNILATTRFYCTFLGNGDKAVFCTVLRIVKLFEDATCTCPHTHKLKMKWRFKQ
HQA
</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see http://www.uniprot.org/terms
Distributed under the Creative Commons Attribution-NoDerivs License
</copyright>
</uniprot>