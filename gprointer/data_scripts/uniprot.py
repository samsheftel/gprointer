import os
from os.path import join as pjoin
pdbdir = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/output'

pdbs = os.listdir(pdbdir)
def pdb_id_getter(pdb):
	return os.path.splitext(os.path.split(pdb)[1])[0]
pdb_and_p0_dict = {}
for pdb in pdbs:
	pdb_p0_codes = set()
	full_pdb_path = pjoin(pdbdir, pdb)
	pdb_id = pdb_id_getter(full_pdb_path)
	with open(full_pdb_path, 'r') as f:
		for line in f:
			if 'DBREF' in line:
				if 'P0' in line:
					po_start = line.find('P0')
					po_end = line.find(' ', po_start)
					po_code = line[po_start:po_end]
					pdb_p0_codes.add(po_code)
	pdb_and_p0_dict[pdb_id] = pdb_p0_codes

pdb_and_p0_dict_filtered = {pdb_id:list(p0_set)[0] for pdb_id, p0_set in pdb_and_p0_dict.items() if p0_set and len(p0_set)<2}
print(pdb_and_p0_dict_filtered.items())
#p0 names saved to /Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/pdb_and_p0.json




