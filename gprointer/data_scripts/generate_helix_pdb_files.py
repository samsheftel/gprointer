import json
import os
import pdb_subset

helix_pdb_dir = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/alpha_helix_chain_pdb'
pdb_dir = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/output'
helix_span_data_json = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_data_from_p0_xml.json'
helix_pdb_file_name_pattern = "%s_Helix_%s.pdb"
all_helix_pdb_file_name_patter = "%s_All7Helixes.pdb"

def json_from_file(file_path):
	with open(file_path, 'r') as f:
		data = json.load(f)
	return data
def get_pdb_file(pdb_id, pdb_dir=pdb_dir):
	return open(os.path.join(pdb_dir, pdb_id+'.pdb'), 'r')

data = {}
helix_span_data = json_from_file(helix_span_data_json)
for pdb_helix_span in helix_span_data:
	pdb_id = pdb_helix_span[u'pdb_id']
	data[pdb_id] = {}
	# pdb_file_obj = get_pdb_file(pdb_id)
	helix_subset_lines  = {}
	for helix in pdb_helix_span[u'helixes']:
		pdb_file_obj = get_pdb_file(pdb_id)
		helix_num = helix[u'helix_num']
		location_begin = helix[u'location_begin']
		location_end = helix[u'location_end']
		out, chain, residues = pdb_subset.pdbSubset(pdb_file_obj, "A", (location_begin, location_end) )
		data[pdb_id][helix_num] = out
		pdb_file_obj.close()

#print(data['1F88'][1][0])

with open('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_pdb_line_data.json', 'w') as f:
	json.dump(data, f, indent=4)
#writes to /Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_pdb_line_data.json
#data will be the form Dict<pdb_id, Dict<helixnum, List<pdb line strings>>>>

##########################################
###Write hekix sections to pdb files######
##########################################


all_posible_atom_list = ["ATOM  ","ANISOU","HETATM","TER   "]
only_atom_posible_atom_list = ["ATOM  "]

for pdb_id, helix_dict in data.items():
	cur_pdb_dir = os.path.join(helix_pdb_dir, pdb_id)
	if not os.path.exists(cur_pdb_dir): os.mkdir(cur_pdb_dir)
	helix_atom_only_lines = []#THIS IS TO GENREATE THE COMBO LIST OF HELIXES
	for helix_num, pdb_lines in helix_dict.items():
		cur_helix_file_name = helix_pdb_file_name_pattern % (pdb_id, str(helix_num))
		cur_helix_file_path = os.path.join(cur_pdb_dir, cur_helix_file_name)
		with open(cur_helix_file_path, 'w') as f:
			for pdb_line in pdb_lines:
				if pdb_line[0:6] in only_atom_posible_atom_list:##CHANGE ME TO all_posible_atom_list INCLUDE HETEROATOMS!!!!!!!!!
					helix_atom_only_lines.append(pdb_line)
				f.write(pdb_line)
	##WRITES THE COMBO FILE WITH ALL 7 Helixes
	compbo_helix_file_name = all_helix_pdb_file_name_patter % pdb_id
	compbo_helix_file_path = os.path.join(cur_pdb_dir, compbo_helix_file_name)
	with open(compbo_helix_file_path, 'w') as f:
		for line in helix_atom_only_lines:
			f.write(line)
		f.write('END\n')





