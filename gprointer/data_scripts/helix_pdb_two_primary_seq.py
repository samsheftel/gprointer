from utils import parserPdb, AMINO_ACID_ABV
import path
import json
from utils import writeJson, openJson


CHAIN_ROOT_DIR = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/alpha_helix_chain_pdb"
PRIMARY_SEQ_DATA_JSON_FILE_WITH_RES_NUM = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_primary_seq_WITH_ResNum_data.json'
PRIMARY_SEQ_DATA_JSON_FILE = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_primary_seq.json'

pdb = parserPdb('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/alpha_helix_chain_pdb/1GZM/1GZM_Helix_1.pdb')



data = {}
for helix_file_path in path.path(CHAIN_ROOT_DIR).walkfiles():
	pdb_id = str(helix_file_path.dirname().name)
	if not data.get(pdb_id, None): data[pdb_id] = {}
	index = helix_file_path.find('Helix_')
	if index == -1:continue
	helix_num = int(helix_file_path[index+6])
	pdb = parserPdb(str(helix_file_path))
	primary_seq = []
	for res in pdb.get_residues():
		res_abv = res.get_resname()
		res_char = AMINO_ACID_ABV[res_abv]
		res_num = res.get_id()[1]
		primary_seq.append( (res_num, res_char) )
	data[pdb_id][helix_num] = primary_seq

#Writes data to json file
#the Format Of the Data will be
#Dict<pdb_id, Dict<chain_num, List<Tuple<res_num, res_char>>>>
writeJson(data, PRIMARY_SEQ_DATA_JSON_FILE_WITH_RES_NUM)

data_seq_only = {}
for pdb_id in data:
	pdb_seqes = []
	for i in range(1,8):
		running_seq = []
		for res_num, res_char in data[pdb_id][i]:
			running_seq.append(res_char)
		running_seq_str = ''.join(running_seq)
		pdb_seqes.append(running_seq_str)
	data_seq_only[pdb_id] = pdb_seqes
print(data_seq_only)

##Write data_seq_only to json file
#the format will be 
#Dict<pdb_id, List<primary_seq_str>>
#where the List<primary_seq_str> will be of len 7 ie one for each helix
#in order
writeJson(data_seq_only, PRIMARY_SEQ_DATA_JSON_FILE)



