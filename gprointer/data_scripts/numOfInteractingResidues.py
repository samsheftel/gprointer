"""
Calulates teh number of residues that each residue in the gprotien interacts with

"""
import re
import json
import os
import csv
PYMOLSELECTIONPATTERN = re.compile('/([\w\d]+)//(\w)/((\w+)`(\d+))(/(\w+)?)?')
ResNumIdex = [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342]

def parse_pymol_format(pymol_format_str):
	""" converts pdb format strig (res or atom spesification) to tuple of form
		struct, chain, resname, resnum, atomname

	>>> parse_pymol_format("/2RH1_edited//A/GLU`187/C")
	('2RH1_edited', 'A', 'GLU', 187, 'C')
	>>> parse_pymol_format("/2RH1_edited//A/GLU`187")
	('2RH1_edited', 'A', 'GLU', 187, None)

	"""
	matchobj = PYMOLSELECTIONPATTERN.match(pymol_format_str.strip())
	struct, chain, res, resname, resnum, slash, atomname = matchobj.groups()
	resnum = int(resnum) if resnum.isdigit() else resnum
	return struct, chain, resname, resnum, atomname


def savetocsv(headerrow, rows, fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'w') as f:
		csv_writer = csv.writer(f, dialect='excel')
		csv_writer.writerow(headerrow)
		for row in rows:
			csv_writer.writerow(row)
	return fpath


def loadjson(fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'r') as f:
		data = json.load(f)
	return data

def resNumFromPymolStr(pymol_format_str):
	return parse_pymol_format(pymol_format_str)[3]


def pymolRes2ResInter(csudata):
	csudata = csudata.get("ResidueData", csudata)
	for res1, interdict in csudata.items():
		ress=[]
		for res2, interaction in interdict.items():
			ress.append(res2)
		yield (res1, ress)


proNames = """2RH1
3D4S
3NY8
3NY9
3NYA
3NYA_MCMM
3P0G
3PDS""".splitlines()

proPath = """/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/2RH1/2RH1_eo_nohet_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/3D4S/3D4S_eo_nohet_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/3NY8/3NY8_eo_nohet_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/3NY9/3NY9_eo_nohet_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/3NYA/3NYA_eo_nohet_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/PreNIH/nol/3NYA_MCMM_nol_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/3P0G/3P0G_eo_nohet_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/NoLigand/3PDS/3PDS_eo_nohet_csu_data.json""".splitlines()

proNameAndPath = dict(zip(proNames, proPath))
##

##

def DegreeCentralityNoLigand():
	degreeCentralityData = {}
	for proname, propath in proNameAndPath.items():
		degreecent = {}
		csudata = loadjson(propath)["ResidueData"]
		for sourceresnum, conresesnum in pymolRes2ResInter(csudata):
			#degreecent[sourceresnum] = len(conresesnum)
			degreecent[resNumFromPymolStr(sourceresnum)] = len(conresesnum)
		degreeCentralityData[proname] = degreecent

	#print(degreeCentralityData)
	rows = [["resnum"] + proNames]
	for resnum in ResNumIdex:
		row = [resnum]
		for proname in proNames:
			row.append( degreeCentralityData.get(proname, {}).get(resnum, 0) )
		rows.append(row)

	print(rows)

	#Degree-centrality
	outpath = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/Results/degree_centrality/degree_centrality_without_ligand.csv"
	savetocsv(rows[0], rows[1:], outpath)


proNames_L = """2RH1_L
3D4S_L
3NY8_L
3NY9_L
3NYA_L
3NYA_L_MCMM
3P0G_L
3PDS_L""".splitlines()

proPath_L = """/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/2RH1/2RH1_eo_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/3D4S/3D4S_eo_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/3NY8/3NY8_eo_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/3NY9/3NY9_eo_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/3NYA/3NYA_eo_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/PreNIH/wl/3NYA_MCMM_wl_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/3P0G/3P0G_eo_csu_data.json
/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/3PDS/3PDS_eo_csu_data.json""".splitlines()

proNameAndPath_L = dict(zip(proNames_L, proPath_L))

def DegreeCentralityWithLigands():
	degreeCentralityData = {}
	for proname, propath in proNameAndPath_L.items():
		degreecent = {}
		csudata = loadjson(propath)["ResidueData"]
		for sourceresnum, conresesnum in pymolRes2ResInter(csudata):
			#degreecent[sourceresnum] = len(conresesnum)
			rn = resNumFromPymolStr(sourceresnum) 
			rn = rn if rn <= 342 else 1000 
			degreecent[rn] = len(conresesnum)
		degreeCentralityData[proname] = degreecent

	#print(degreeCentralityData)
	rows = [["resnum"] + proNames]
	ResNumIdexPlusLig = ResNumIdex + [1000]
	for resnum in ResNumIdexPlusLig:
		row = [resnum]
		for proname in proNames_L:
			row.append( degreeCentralityData.get(proname, {}).get(resnum, 0) )
		rows.append(row)

	print(rows)

	#Degree-centrality
	outpath = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/Results/degree_centrality/degree_centrality_with_ligand.csv"
	savetocsv(rows[0], rows[1:], outpath)
DegreeCentralityWithLigands()



#csudata = loadjson('/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo/Ligand/2RH1/2RH1_eo_csu_data.json')
#csuresdata = csudata["ResidueData"]
#for res, ress in pymolRes2ResInter(csuresdata):
#	print(str(resNumFromPymolStr(res))+' '+ str(map(resNumFromPymolStr, ress)))
## for k in csuresdata.keys():
## 	print(str(parse_pymol_format(k)[3])+' '+ str(len(csuresdata[k])))

