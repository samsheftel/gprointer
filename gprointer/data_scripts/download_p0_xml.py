import json
import os
from os.path import join as pjoin
import shutil
import urllib

p0_xml_url_pat = 'http://www.uniprot.org/uniprot/%s.xml'

pdb_and_p0_json_file = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/pdb_and_p0.json'
with open(pdb_and_p0_json_file, 'r') as f:
	pdb_and_p0_dict_list = json.load(f)
print(pdb_and_p0_dict_list)


p0_out_dir = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/p0_xml_files'
p0_xml_file_pat = '%s_p0xml_%s.xml'

for pdb_and_p0_dict in pdb_and_p0_dict_list:
	pdb_id, p0_id = str(pdb_and_p0_dict['pdb_id']), str(pdb_and_p0_dict['p0_id'])
	save_file_name = p0_xml_file_pat % (pdb_id, p0_id)
	xml_file_obj = urllib.urlopen(p0_xml_url_pat % p0_id)
	with open(pjoin(p0_out_dir, save_file_name), 'w') as f:
		print(type(f))
		print(dir(xml_file_obj))
		shutil.copyfileobj(xml_file_obj, f)
#files saved to /Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/p0_xml_files

