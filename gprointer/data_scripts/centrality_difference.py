#!/usr/bin/env python
"""
takes two centrality data files with the same residue index and returnes a
comparitive centrality file.

Define cList = list of rescentrality

readcentralityCsv(csvfile) -> cList

readcentralityJson(jsonfile) -> cList

filterCentrality(callable) -> list of 

ge
"""
import csv
import os
import operator
import warnings
def readcentralityCsv(fpath):
	with open( fpath, 'r') as f:
		reader = csv.reader(f)
		reader.next()
		l = []
		for row in reader:
			 l.append(ResCentrality(*row))
	return l



class ResCentrality(object):
	__slots__ = ['pymol', 'resnum', 'resname', 'region_name', 'ballesteros', 'centrality_score']
	def __init__(self, pymol, resnum, resname, region_name, ballesteros, centrality_score):
		self.pymol = str(pymol)
		self.resnum = int(resnum)
		self.resname = str(resname)
		self.region_name = str(region_name)
		self.ballesteros = str(ballesteros)
		self.centrality_score = float(centrality_score)
	def __repr__(self):
		attrvals = ', '.join(
			[repr(getattr(self, attrkey)) for attrkey in self.__slots__])
		reprstr =  self.__class__.__name__ + '('+ attrvals + ')'
		return reprstr
	@property
	def pymolg(self):
		return '////`%i/' % self.resnum
	def __sub__(self, other):
		if self.resnum != other.resnum:
			warnings.warn(
				'res %i from %s numbers not matchup with %i of %s' % (
					self.resnum, self.pymol, other.resnum, other.pymol
				)
			)
			return None
		resname = self.resname if self.resname == other.resname else ""
		region_name = self.region_name if self.region_name == other.region_name else ""
		ballesteros = self.ballesteros if self.ballesteros == other.ballesteros else ""
		centrality_score_dif = self.centrality_score - other.centrality_score
		return self.__class__(self.pymolg, self.resnum, resname, region_name, ballesteros, centrality_score_dif)

	def asList(self):
		"""returns a list of the sloted atribut values"""
		outList = []
		for slotattr in self.__slots__:
			outList.append(getattr(self, slotattr))
		return outList






class CentList(object):
	def __init__(self, *rescent):
		self.cents = rescent
		self.resnum_map = dict([(cent.resnum, idx)for idx, cent in enumerate(self.cents)])
	def __len__(self):
		return len(self.cents)
	def __iter__(self):
		return iter(self.cents)
	def __repr__(self):
		cents_as_str = ', '.join([repr(rescent) for rescent in self.cents])
		reprstr =  self.__class__.__name__ + '('+ cents_as_str + ')'
		return reprstr
	@classmethod
	def fromCsv(cls, csvfile):
		csvfile = os.path.abspath(csvfile)
		rows = []
		f = open(csvfile, 'r')
		reader = csv.reader(f)
		reader.next()
		for centdata in reader:
			rows.append(ResCentrality(*centdata))
		f.close()
		return cls(*rows)
	def _filter(self, callable_filter):
		outl = []
		for resCentrality in self.cents:
			if callable_filter(resCentrality):
				outl.append(resCentrality)
		return outl

	def __sub__(self, other_cent_list):
		"""other is a CentList will return a new CentList"""
		resCentralityDifs = []
		for rescent1 in self.cents:
			rescent2 = other_cent_list[rescent1]
			if not rescent2:
				warnings.warn(
					'res %i from %s numbers not other_cent_list no sub returned' % (
						rescent1.resnum, rescent1.pymol
					)
				)
				continue
			resCentralityDifs.append(rescent1-rescent2)
		return self.__class__(*resCentralityDifs)


	def __getitem__(self, resnum_or_rescentobj):
		if isinstance(resnum_or_rescentobj, ResCentrality):
			resnum_or_rescentobj = resnum_or_rescentobj.resnum
		resnum_index_in_cents = self._hasresnumandindex(resnum_or_rescentobj)
		if resnum_index_in_cents or resnum_index_in_cents == 0:
			return self.cents[resnum_index_in_cents]
		return None
		return self.cents[self.resnum_map[resnum_or_rescentobj]]
	
	def _hasresnumandindex(self, resnum):
		return self.resnum_map.get(resnum, None)

	def minCent(self):
		"""returns the rescent with the smallest centrality score"""
		minsofar, minrescent = self.cents[0].centrality_score, self.cents[0]
		for cent in self.cents:
			if cent.centrality_score < minsofar:
				minsofar = cent.centrality_score
				minrescent = cent
		return minrescent
	def maxCent(self):
		maxsofar, maxrescent = self.cents[0].centrality_score, self.cents[0]
		for cent in self.cents:
			if cent.centrality_score > maxsofar:
				maxsofar = cent.centrality_score
				maxrescent = cent
		return maxrescent

	def sortCent(self):
		sortedcents = sorted(self.cents, key=operator.attrgetter('centrality_score'))
		return self.__class__(*sortedcents)
	def getTop20percent(self):
		"""returns a list of resCents which are the top 20%"""
		index_of_top_20_percent = int(len(self.cents)*.2)
		sortedcents = self.sortCent()
		return sortedcents.cents[:index_of_top_20_percent]
	def get20percentCutOffNum(self):
		"""returns the centraliy value whihc is at the 20th% tile"""
		return self.getTop20percent()[-1].centrality_score
	def centBigerThen(self, cutoffnum=None):
		"""retursn list of resCents which have a centralit_score biggern then cutoffnum"""
		cutoffnum = cutoffnum if cutoffnum else self.get20percentCutOffNum()
		#defaults the cutoff biger then to the 20% value
		biggerthenList = []
		for rescent in self.cents:
			if rescent.centrality_score >= cutoffnum:
				biggerthenList.append(rescent)
		return biggerthenList

	def writeToCsv(self, fpath):
		rows = [resCent.asList() for resCent in self.cents]
		headerrow = self.cents[0].__slots__
		savetocsv(headerrow, rows, fpath)














if __name__ == '__main__':
	csvfile1 = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/CostanziEmail/Beta2_AR/3P0G_2RH1_EditedOren/2RH1_edited_oriented_centrality_data.csv"
	csvfile2 = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/CostanziEmail/Beta2_AR/3P0G_2RH1_EditedOren/3P0G_edited_oriented_centrality_data.csv"
	centlist_csv1 = CentList.fromCsv(csvfile1)
	centlist_csv2 = CentList.fromCsv(csvfile2)
	# print(centlist_csv2 - centlist_csv1)
	# print(centlist_csv1 - centlist_csv2)
	print centlist_csv2.get20percentCutOffNum()
	print centlist_csv2.centBigerThen()
	print(centlist_csv2.maxCent())
	print(centlist_csv2.minCent())

