from path import path
import os
import json
import operator
import copy
import tablib
from collections import namedtuple
import csv

"""
This file is the script used to run the following calulations/experiments


note set 1000 to index of ligan

delta Centrality Cutoff cutoff +-0.02

2RH1 - all
2RH1_L - all_L
Note 
	2RH1 - 3P0G will be different from rest

------------
delta Centrality cutoff +-0.015, also try cutoff +-0.015
	matching: all_L - all

----------------
Centrality cutoff +0.25
	all
	all_L
-----------

color by tm in pymol
hide lines show cartoon
-----------------



"""

DATASETROOT = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/data/datasets/BetaAdrenergicReceptor/AnalysisSetTwo"
datasetroot = path(DATASETROOT)

OUTDIR = "/Users/sam/Downloads"

def savetocsv(headerrow, rows, fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'w') as f:
		csv_writer = csv.writer(f, dialect='excel')
		csv_writer.writerow(headerrow)
		for row in rows:
			csv_writer.writerow(row)
	return fpath

def namedtuple_decor(func):
	return namedtuple(func.__name__, func.__code__.co_varnames)

@namedtuple_decor
def Centrality(pymol, resnum, resname, region_name, ballesteros, centrality_score):pass

def getkeys(d, *keys):
	values = []
	for k in keys:
		values.append(d[k])
	return values

def getLigandAndNoLigandData(datasetroot):
	noligdict = dict()
	ligdict = dict()
	for folder in datasetroot.dirs():
		if folder.name == "Ligand":
			for profolder in folder.dirs():
				jsonfile = [f for f in profolder.files() if f.ext == '.json'][0]
				ligdict[str(profolder.name)] = str(jsonfile)

		if folder.name == "NoLigand":
			for profolder in folder.dirs():
				jsonfile = [f for f in profolder.files() if f.ext == '.json'][0]
				noligdict[str(profolder.name)] = str(jsonfile)
	return noligdict, ligdict

def loadjson(fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'r') as f:
		data = json.load(f)
	return data

def savetojson(obj, fpath, pretty_print=True):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'w') as f:

		if pretty_print:
			json.dump(obj, f, indent=4)
		else:
			json.dump(obj, f, indent=4)
	return fpath


noligdict, ligdict = getLigandAndNoLigandData(datasetroot)
LigandResNums = set([366, 1207, 1203, 1201, 1203, 408, 401])
LigandIndexInJsonArray = 273

def _2RH1MinusAllCsvOutput():
	#data = {}
	alldata={}
	pathdict = noligdict
	startpro = '2RH1'
	_2RH1Data = loadjson(pathdict['2RH1'])
	for res, datafile in pathdict.items():
		if str(res) == startpro: continue 
		rows = []
		rescent = loadjson(datafile)
		name = startpro + '-' + str(res) + '_delta_centrality'
		for i in range(len(_2RH1Data)):
			row = getkeys(_2RH1Data[i], "pymol", "resnum", "resname", "region_name", "ballesteros", "centrality_score")
			row[-1] = row[-1] - rescent[i]['centrality_score']
			rows.append(row)
		alldata[name] = rows
		headers = ["pymol", "resnum", "resname", "region_name", "ballesteros", "centrality_score"]
		#data = tablib.Dataset(*rows, headers=headers)
		outcsvname = name+'.csv'
		outfilepath = os.path.join(OUTDIR, outcsvname)
		savetocsv(headers, rows, outfilepath)
		print('created file %s' % outfilepath)
		#print(data.csv)
		#print('\n\n\n')
	#return data

	_2rh1data = loadjson(pathdict["2RH1"])
	zc = zip(*[[r[-1] for r in v] for v in alldata.values()])
	comborows = []
	print(len(_2rh1data))
	print(len(zc))
	#print(zc)
	if len(_2rh1data) == len(zc):
		for cnt, dd in enumerate(_2rh1data):
			rrow = getkeys(_2rh1data[cnt], "pymol", "resnum", "resname", "region_name", "ballesteros")
			rrow.extend(zc[cnt])
			comborows.append(rrow)
		headercombo = ["pymol", "resnum", "resname", "region_name", "ballesteros"]
		for nm in alldata.keys():
			headercombo.append(nm)
		outcsvname = "summary_delta_centrality_vs_2RH1"+'.csv'
		outfilepath = os.path.join(OUTDIR, outcsvname)
		savetocsv(headercombo, comborows, outfilepath)
		print('created file %s' % outfilepath)

def _2RH1MinusAll():
	data = {}
	pathdict = noligdict
	startpro = '2RH1'
	_2RH1Data = loadjson(pathdict['2RH1'])
	for res, datafile in pathdict.items():
		if str(res) == startpro: continue 
		deltasdict = {}
		rescent = loadjson(datafile)
		name = startpro + '-' + str(res) + '_delta_centrality'
		for i in range(len(_2RH1Data)):
			deltasdict[_2RH1Data[i]['resnum']] = _2RH1Data[i]['centrality_score'] - rescent[i]['centrality_score']
		data[name] = deltasdict
	return data

def _2RH1MinusAllWithLigand():
	data = {}
	pathdict = ligdict
	startpro = '2RH1'
	_2RH1Data = loadjson(pathdict['2RH1'])
	for res, datafile in pathdict.items():
		if str(res) == startpro: continue 
		deltasdict = {}
		rescent = loadjson(datafile)
		name = startpro + '_L-' + str(res)+"_L" + '_delta_centrality'
		for i in range(len(_2RH1Data)):
			if _2RH1Data[i]['resnum'] == 408:
				deltasdict[1000] = _2RH1Data[i]['centrality_score'] - rescent[i]['centrality_score']
			else:
				deltasdict[_2RH1Data[i]['resnum']] = _2RH1Data[i]['centrality_score'] - rescent[i]['centrality_score']
		data[name] = deltasdict
	return data




def _2RH1MinusAllWithLigandCsvOutpute():
	#data = {}
	alldata={}
	pathdict = ligdict
	startpro = '2RH1'
	_2RH1Data = loadjson(pathdict['2RH1'])
	for res, datafile in pathdict.items():
		if str(res) == startpro: continue 
		rows = []
		rescent = loadjson(datafile)
		name = startpro + '_L-' + str(res)+"_L" + '_delta_centrality'
		for i in range(len(_2RH1Data)):
			row = getkeys(_2RH1Data[i], "pymol", "resnum", "resname", "region_name", "ballesteros", "centrality_score")

			# if _2RH1Data[i]['resnum'] == 408:
			# 	deltasdict[1000] = _2RH1Data[i]['centrality_score'] - rescent[i]['centrality_score']
			# else:
			# 	deltasdict[_2RH1Data[i]['resnum']] = _2RH1Data[i]['centrality_score'] - rescent[i]['centrality_score']
			row[-1] = row[-1] - rescent[i]['centrality_score']
			rows.append(row)
		alldata[name] = rows
		headers = ["pymol", "resnum", "resname", "region_name", "ballesteros", "centrality_score"]
		outcsvname = name+'.csv'
		outfilepath = os.path.join(OUTDIR, outcsvname)
		savetocsv(headers, rows, outfilepath)
		print('created file %s' % outfilepath)

	_2rh1data = loadjson(pathdict["2RH1"])
	zc = zip(*[[r[-1] for r in v] for v in alldata.values()])
	comborows = []
	print(len(_2rh1data))
	print(len(zc))
	#print(zc)
	if len(_2rh1data) == len(zc):
		for cnt, dd in enumerate(_2rh1data):
			rrow = getkeys(_2rh1data[cnt], "pymol", "resnum", "resname", "region_name", "ballesteros")
			rrow.extend(zc[cnt])
			comborows.append(rrow)
		headercombo = ["pymol", "resnum", "resname", "region_name", "ballesteros"]
		for nm in alldata.keys():
			headercombo.append(nm)
		outcsvname = "summary_delta_centrality_vs_2RH1_L"+'.csv'
		outfilepath = os.path.join(OUTDIR, outcsvname)
		savetocsv(headercombo, comborows, outfilepath)
		print('created file %s' % outfilepath)


def deltacentralityMatchingLigVsNonlig():
	data = {}
	for proname in ligdict:
		prodata_lig_data = loadjson(ligdict[proname])
		prodata_nonlig_data = loadjson(noligdict[proname])
		centname = proname +"_L"+'-'+proname+'_delta_centrality'
		cur = {}
		for i in range(len(prodata_nonlig_data)):
			resnum = prodata_nonlig_data[i]['resnum']
			ligcs = prodata_lig_data[i]['centrality_score']
			nlcs = prodata_nonlig_data[i]['centrality_score']
			deltac = ligcs - nlcs
			cur[resnum]= deltac
		data[centname] = cur
	return data


def deltacentralityMatchingLigVsNonligCsvOutpute():
	data = {}
	alldata = {}
	headers = ["pymol", "resnum", "resname", "region_name", "ballesteros", "centrality_score"]
	for proname in ligdict:
		prodata_lig_data = loadjson(ligdict[proname])
		prodata_nonlig_data = loadjson(noligdict[proname])
		centname = proname +"_L"+'-'+proname+'_delta_centrality'
		cur = {}
		rows = []
		for i in range(len(prodata_nonlig_data)):
			row = getkeys(prodata_nonlig_data[i], "pymol", "resnum", "resname", "region_name", "ballesteros", "centrality_score")
			#resnum = prodata_nonlig_data[i]['resnum']
			ligcs = prodata_lig_data[i]['centrality_score']
			nlcs = prodata_nonlig_data[i]['centrality_score']
			deltac = ligcs - nlcs
			row[-1] = deltac
			rows.append(row)
		#data[centname] = cur
		alldata[centname] = rows
		
		outcsvname = centname+'.csv'
		outfilepath = os.path.join(OUTDIR, outcsvname)
		savetocsv(headers, rows, outfilepath)
		print('created file %s' % outfilepath)

	#return data
	_2rh1data = loadjson(noligdict["2RH1"])
	zc = zip(*[[r[-1] for r in v] for v in alldata.values()])
	comborows = []
	#print(zc)
	if len(_2rh1data) == len(zc):
		for cnt, dd in enumerate(_2rh1data):
			rrow = getkeys(_2rh1data[cnt], "pymol", "resnum", "resname", "region_name", "ballesteros")
			rrow.extend(zc[cnt])
			comborows.append(rrow)
		headercombo = ["pymol", "resnum", "resname", "region_name", "ballesteros"]
		for nm in alldata.keys():
			headercombo.append(nm)
		outcsvname = "summary_delta_centrality_with_ligand_vs_without"+'.csv'
		outfilepath = os.path.join(OUTDIR, outcsvname)
		savetocsv(headercombo, comborows, outfilepath)
		print('created file %s' % outfilepath)





def nonLiganWithCentralityGreater(num):
	for proid, profile in noligdict.items():
		print('%s with centrality bigger then %s' % (proid, str(num)))
		procentdata = loadjson(profile)
		for centdata in procentdata:
			if centdata['centrality_score'] >= num:
				print(str(centdata['resnum'])+" "+str(centdata['centrality_score']))
		print('\n\n')

def liganWithCentralityGreater(num):
	for proid, profile in ligdict.items():
		print('%s with centrality bigger then %s' % (proid, str(num)))
		procentdata = loadjson(profile)
		for centdata in procentdata:
			if centdata['centrality_score'] >= num:
				print(str(centdata['resnum'])+" "+str(centdata['centrality_score']))
		print('\n\n')


def centfiltermaker(cuttoff, pm='+'):
	if pm == '-':
		def ffn(detlattup):
			return detlattup[1]<= 0 and abs(detlattup[1]) >= cuttoff
	else:
		def ffn(detlattup):
			return detlattup[1] >= cuttoff
	return ffn




if __name__ == '__main__':
	##print(_2RH1MinusAll())
	##print(_2RH1MinusAllWithLigand())
	##print(deltacentralityMatchingLigVsNonlig())
	##for k,v in _2RH1MinusAll().items():
	##for k,v in _2RH1MinusAllWithLigand().items():
	# for k,v in deltacentralityMatchingLigVsNonlig().items():
	# 	print(k+str(' +0.02'))
	# 	for resnum, cent in filter(centfiltermaker(0.02, '+'), v.items()):
	# 		print(str(resnum)+'  '+ str(cent))
	# 	print(k+str(' -0.02'))
	# 	for resnum, cent in filter(centfiltermaker(0.02, '-'), v.items()):
	# 		print(str(resnum)+'  '+ str(cent))
	# 	print('\n\n')
	
	#nonLiganWithCentralityGreater(0.25)
	#liganWithCentralityGreater(0.25)
	#print(_2RH1MinusAll().keys())
	#print(_2RH1MinusAllWithLigand().keys())

	#######
	#_2RH1MinusAllCsvOutput()
	#_2RH1MinusAllWithLigandCsvOutpute()
	#print(deltacentralityMatchingLigVsNonlig().keys())
	
	#deltacentralityMatchingLigVsNonligCsvOutpute()

	#_2RH1MinusAllWithLigandCsvOutpute()

	_2RH1MinusAllCsvOutput()







