
import path
#import simplexml
import xmltodict
from pprint import pprint
import json
p0_xml_dir_str = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/p0_xml_files'
def parse_p0_file_name(p0_file_path_obj):
	pdb_id, p0_id = str(p0_file_path_obj.name).split('_p0xml_')
	return (pdb_id,p0_id[:-4])

p0_xml_dir = path.path(p0_xml_dir_str)
data = []
for p0_xml_file_path in p0_xml_dir.listdir():
	pdb_id, p0_id = parse_p0_file_name(p0_xml_file_path)
	helix_data = dict(pdb_id=pdb_id, p0_id=p0_id, helixes=[])
	with open(p0_xml_file_path, 'r') as f:
		xmltext = f.read()
	xml_dict = xmltodict.parse(xmltext)
	for feature_node in xml_dict[u'uniprot'][u'entry'][u'feature']:
		if feature_node[u'@type'] == u'transmembrane region':
			#print(feature_node)
			description = feature_node[u'@description']
			helix_num = int(description.replace('Helical; Name=', ''))
			location = feature_node[u'location']
			location_begin = int(location[u'begin'][u'@position'])
			location_end = int(location[u'end'][u'@position'])

			helix_start_end = {"helix_num":helix_num, 'location_begin':location_begin, 'location_end':location_end}
			helix_data['helixes'].append(helix_start_end)
	if len(helix_data['helixes']) != 7:
		print('bad data helix not 7 helixes in %s count= %i \n will not be included' % (pdb_id, len(helix_data['helixes']) ))
	data.append(helix_data)
			# print(description)
			# print(location)
			# print('\n')
			# #print(feature_node[u'@description'])
#print(len(data))

out_data_file = '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_data_from_p0_xml.json'
with open(out_data_file, 'w') as f:
	json.dump(data, f, indent=4)

#save data to /Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_data_from_p0_xml.json
