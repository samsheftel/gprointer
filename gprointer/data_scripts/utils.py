from Bio.PDB.PDBParser import PDBParser
import os
import json

AMINO_ACID_ABV = {'ILE': 'I', 'GLN': 'Q', 'XLE': 'J', 'GLX': 'Z', 'GLY': 'G', 'GLU': 'E', 'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'LYS': 'K', 'PRO': 'P', 'ASX': 'B', 'SEC': 'U', 'ASN': 'N', 'VAL': 'V', 'THR': 'T', 'HIS': 'H', 'TRP': 'W', 'PHE': 'F', 'ALA': 'A', 'MET': 'M', 'PYL': 'O', 'LEU': 'L', 'ARG': 'R', 'TYR': 'Y'}

def writeJson(obj, path, prettyprint=True):
	with open(path, 'w') as f:
		if prettyprint:
			json.dump(obj, f, indent=4)
		else:
			json.dump(obj, f)
	return os.path.abspath(path)

def openJson(path):
	with open(path, 'r') as f:
		data = json.load(f)
	return data


def getPdbPathAndProteinName(pdbFileObjOrPath):
    """
    takes either a pdb file path as a string or a file obj, returns the path of the pdb
        and the name of the Structure

    >>> getPdbPathAndProteinName(PDBFILE1)
    ('/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/2RH1.pdb', '2RH1')
    >>> testopenfile = open(PDBFILE1,'r')
    >>> getPdbPathAndProteinName(testopenfile)
    ('/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/2RH1.pdb', '2RH1')
    >>> testopenfile.close()
    """
    if isinstance(pdbFileObjOrPath, file):
        pdbFileObjOrPath = pdbFileObjOrPath.name
    absolutePdbFilePath = os.path.abspath(pdbFileObjOrPath)
    pdbFileDir, pdbFileName = os.path.split(absolutePdbFilePath)
    proteinName, pdbFileExtention = os.path.splitext(pdbFileName)
    return (absolutePdbFilePath, proteinName)

def parserPdb(pdbFileObjOrPath):
    """
    returns the Biopython pdb object for the pdb file | file obj passed in

    >>> parserPdb(PDBFILE1)
    <Structure id=2RH1>
    >>> testopenfile = open(PDBFILE1,'r')
    >>> parserPdb(testopenfile)
    <Structure id=2RH1>
    >>> testopenfile.close()
    """
    parser = PDBParser()
    absolutePdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
    return parser.get_structure(proteinName, absolutePdbFilePath)