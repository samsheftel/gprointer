
import re
from utils import writeJson, openJson

PRIMARY_SEQ_DATA_FILE = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_primary_seq.json"
PRIMARY_SEQ_WITH_RESNUM_DATA_FILE = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/helix_primary_seq_WITH_ResNum_data.json"

OUT_FILE_RESIDUE_NUMBER_OF_MOST_CONSERVED = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/res_num_of_most_conserved_ballesteros.json"
OUT_FILE_RESIDUE_NUMBER_TO_BALLESTEROS_NUMBER_MAPPING = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/generated_data/res_num_to_ballesteros_num.json"

#find_ ballesteros_numbering_map.py
AA_Symbols = ['A', 'C', 'B', 'E', 'D', 'G', 'F', 'I', 'H', 'K', 'J', 'M', 'L', 'O', 'N', 'Q', 'P', 'S', 'R', 'U', 'T', 'W', 'V', 'Y', 'Z']
WildCardAA = '[ACBEDGFIHKJMLONQPSRUTWVYXZ]'

def protien_pattern_matcher(regexstr):
	""""""
	return re.compile(regexstr.replace('X', WildCardAA))


#ORIGONAL PATTERS PRODUCED FROM ARTICAL
#['GN|GX{3}N', '[NSH]LX{3}DX{7,9}P', 'SX{3}LX{2}IX{2}[DEH]RY', 'WX{8,9}P', 'FX{2}PX{7}Y', 'FX{2}C[WYF]XP', 'LX{3}NX{3}[ND]PX{2}YX{5,6}F' ,'LX{3}NX{3}[ND]PX{2}YX{5,6}F']
helix_1_pattern = protien_pattern_matcher('G(X{3})?(?P<id>N)')# 'G(?P<id>N)|GX{3}(?P<id>N)'
helix_1_most_con = 'N'
#helix_2_pattern = protien_pattern_matcher('[NSH]LX{3}DX{7,9}P')
helix_2_pattern = protien_pattern_matcher('[NSH]LX{3}(?P<id>D)(X{7,9}P)?')# made X{7,9}P conditional
helix_2_most_con = 'D'
helix_3_pattern = protien_pattern_matcher('SX{3}LX{2}IX{2}[DEH](?P<id>R)Y')
helix_3_most_con = 'R'
helix_4_pattern = protien_pattern_matcher('(?P<id>W)X{8,9}P')
helix_4_most_con = 'W'
helix_5_pattern = protien_pattern_matcher('FX{2}(?P<id>P)X{7}Y')
helix_5_most_con = 'P'
helix_6_pattern = protien_pattern_matcher('FX{2}C[WYF]X(?P<id>P)')
helix_6_most_con = 'P'
#helix_7_pattern = protien_pattern_matcher('LX{3}NX{3}[ND]PX{2}YX{5,6}F')
helix_7_pattern = protien_pattern_matcher('[ND](?P<id>P)X{2}Y')# REMOVED LX{3}NX{3}[ and X{5,6}F part 
helix_7_most_con = 'P'
helix_8_pattern = protien_pattern_matcher('LX{3}NX{3}[ND]PX{2}YX{5,6}(?P<id>F)')#????????? se paper no underline
helix_8_most_con = 'F'

helix_pattern_list = [helix_1_pattern, helix_2_pattern, helix_3_pattern, helix_4_pattern, helix_5_pattern, helix_6_pattern, helix_7_pattern, helix_8_pattern]

most_con_list = [helix_1_most_con, helix_2_most_con, helix_3_most_con, helix_4_most_con, helix_5_most_con, helix_6_most_con, helix_7_most_con, helix_8_most_con]



#Dict<pdb_id, List<primary_seq_str>>    where the List<primary_seq_str> will be of len 7 ie one for each helix
pdb_seq_dict = openJson(PRIMARY_SEQ_DATA_FILE)
#Dict<pdb_id, Dict<helix_num, List<Tuple<res_num, res_char>>>>
pdb_seq_with_resnum_dict = openJson(PRIMARY_SEQ_WITH_RESNUM_DATA_FILE)


BALLESTEROS_RES_NUM_MAPPINGS = {}#Dict<pdb_id, Dict<helix_num, Dict<res_num, ballesteros_num>>>
BALLESTEROS_RES_NUMS_DICT = {}#Dict<pdb_id, Dict<helix_num, res_num>># the  res_num is the residue number which is anotated as 50


# onlyonce, mult, multbutmatch, issu = 0, 0, 0, 0
for pdb_id, primary_seqs in pdb_seq_dict.items():
	BALLESTEROS_RES_NUM_MAPPINGS[pdb_id] = {}
	BALLESTEROS_RES_NUMS_DICT[pdb_id] = {}
	if len(primary_seqs) != 7: raise EOFError('%s not have 7 helixes' % pdb_id)
	for indx, primary_seq in  enumerate(primary_seqs):
		helix_num = unicode(indx +1)
		pattern = helix_pattern_list[indx]
		most_con = most_con_list[indx]

		#key vars that must be set here are 
		#ballesteros_res_index_in_helix_seq - the index in the helix sequenc at which the ballesteros_res is postioned
		#ballesteros_res_num - the res_num of the residue which is given the #50 annotation
		

		most_con_matching_res_count = primary_seq.count(most_con)
		if not most_con_matching_res_count: print("ISSUE %s helix %s %s not in primary seq:\n\t%s\n" % (pdb_id, helix_num, most_con, primary_seq));continue
		elif most_con_matching_res_count == 1:#if most_con residue type only occures once in the helix
			ballesteros_res_index_in_helix_seq = primary_seq.find(most_con)

			ballesteros_res_num, ballesteros_res_char = pdb_seq_with_resnum_dict[pdb_id][helix_num][ballesteros_res_index_in_helix_seq]

		else:#more then 1 res idetical to most conserved in seq
			search_obj = pattern.search(primary_seq)
			if not search_obj: print("PatternMatch ISSUE %s helix %i %s not in primary seq:\n\t%s\n" % (pdb_id, helix_num, most_con, primary_seq) ); continue
			ballesteros_res_index_in_helix_seq = search_obj.start('id')#get the start index of the name match group id which is alwyas the 50 residue
			ballesteros_res_num, ballesteros_res_char = pdb_seq_with_resnum_dict[pdb_id][helix_num][ballesteros_res_index_in_helix_seq]

		#Now that have `ballesteros_res_index_in_helix_seq` and `ballesteros_res_num` put them into data dicts
		#first generate maping from res_num to ballesteros for the helix
		mappingDict = {}#res_num -> ballesteros num in the helix
		for index_in_helix, res_tup in enumerate(pdb_seq_with_resnum_dict[pdb_id][helix_num]):
			res_num, res_car = res_tup
			mappingDict[res_num] = ((50 - ballesteros_res_index_in_helix_seq) + index_in_helix)
		BALLESTEROS_RES_NUMS_DICT[pdb_id][int(helix_num)] = ballesteros_res_num
		BALLESTEROS_RES_NUM_MAPPINGS[pdb_id][int(helix_num)] = mappingDict
#print(BALLESTEROS_RES_NUMS_DICT)
#print(BALLESTEROS_RES_NUM_MAPPINGS)


writeJson(BALLESTEROS_RES_NUMS_DICT, OUT_FILE_RESIDUE_NUMBER_OF_MOST_CONSERVED)
writeJson(BALLESTEROS_RES_NUM_MAPPINGS, OUT_FILE_RESIDUE_NUMBER_TO_BALLESTEROS_NUMBER_MAPPING)



#print('onlyone = %i, mult= %i, multbutmatch= %s, issu=%i' % (onlyonce, mult, multbutmatch, issu))



questions = """

- ISSUE WITH HELIX 3
	Examples:
		ISSUE 2I37 helix 3 R not in primary seq:
			NLEGFFATLGGEIALWSLVVLAI

		ISSUE 1GZM helix 3 R not in primary seq:
			NLEGFFATLGGEIALWSLVVLAI

		ISSUE 1HZX helix 3 R not in primary seq:
			NLEGFFATLGGEIALWSLVVLAI

- Dealing with gaps
	heliz 2 seq 5 in paper has a gap 23 res in 
	this means that residue 63 in the priamry sequnce will get annotated with the 
	wrong ballesteros.
	as is my code doesnt include the msa so 
	residue 63 - F will get the ballesteros 60 but realy this is the ballesteros
	of the gap its ballesteros num should be 61
"""
