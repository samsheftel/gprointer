from __future__ import with_statement
import cmd
#import _pymol
import _cmd
import util

#
def forceList(arg):
	if not isinstance(arg, list):
		arg = [arg]
	return arg

def resetColor(selstr):
	util.cba(33, selstr, _self=cmd)

def resetShape(selstr):
	cmd.hide("everything", selstr)
	cmd.show("lines", selstr)

def selResNums(resnumlist):
	resnumlist = forceList(resnumlist)
	reses = '+'.join([str(resnum) for resnum in resnumlist])
	selstr = "i. "+ reses
	return selstr

def selElmTypeInRes(resnumlist, elmtypelist):
	elmtypelist = forceList(elmtypelist)
	emls = '+'.join([str(emltype) for emltype in elmtypelist])
	selstr = 'e. ' + emltype + ' and ' + elResNums(selResNums) + selResNums(selResNums)
	return selstr

def mkgreen(selstr):
	cmd.color("green", selstr)

def mkred(selstr):
	cmd.color("red", selstr)

def mkspheres(selstr):
	cmd.show("spheres", selstr)

def mklines(selstr):
	cmd.show("lines", selstr)

def slc(selstr, name):
	cmd.select(name, selstr)
