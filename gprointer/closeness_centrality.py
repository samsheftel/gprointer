#closeness centrality
"""
See:
	

closeness centrality of a node x in a network G of n nodes is defined as 
	CC(x) = (n - 1) / sum([distance(x, y) for y in G if y != x])
	where distance(x, y) is the shortest path langth between node x and node y

for more info see page 1143 of artical

It is assumed that the structure of the graph is as follows
	Node = string = a residue pymol selctor like "/3P0G_edited//A/GLY`102/"
	Interaction = is a dict with interaction data like "Distance"
	Graph = Dict<Node, Dict<Node, Interaction>>

"""
################################################################################
import heapq
from collections import defaultdict
from collections import namedtuple
################################################################################

def namedtuple_decor(func):
	"""decorator for creating a named tuple

	>>> @namedtuple_decor
	... def Person(first_name, last_name, age): pass
	>>> Person("Sam", "Sheftel", 25)
	Person(first_name='Sam', last_name='Sheftel', age=25)
	"""
	return namedtuple(func.__name__, func.__code__.co_varnames)

def uniformDistanceWeightFn(interaction): return 1
def infinet(): return float('inf')
def noneCallable(): return None
def forceList(var): return var if isinstance(var, list) else [var]

def dijkstra(csuInteractionGraph, startNode, interactionWeightFunc=None, interactionConstraintFilter=None):
	"""
	Calulates the shortest path tree from a given start node and a interactionGraph.
		Nodes are Atoms or Residues. 

	interactionConstraintFilter is a callable which expects the interaction data dict
		as a arg. interactionConstraintFilter param is optional for use in screening
		out only spesfic types of interactions.
	
	interactionWeightFunc is a callable which calulates the weight of a interaction.
		it reseves one arg of type interaction data dict. it returns a float or int.
		whith smaller => more lickly to be chosen on next iteration
		arg.
		interactionConstraintFilter
	The retrun is a dict represnting the predicesor tree created from the shortest
		path walk.
	
	>>> djkShortestPathTree = dijkstra(CSU_ATOM_DATA_2RH1, '/2RH1//A/TRP`313/CH2', spatialDistanceWeightFn)
	>>> djkShortestPathTree['/2RH1//A/GLN`1123/O']
	'/2RH1//A/LYS`1124/CB'
	>>> djkShortestPathTree = dijkstra(CSU_RES_DATA_2RH1, '/2RH1//A/TRP`313/', spatialDistanceWeightFn)
	>>> djkShortestPathTree['/2RH1//A/GLN`1123/']
	'/2RH1//A/GLN`1122/'



	"""
	interactionWeightFunc = interactionWeightFunc or uniformDistanceWeightFn
	numberOfNodes = len(csuInteractionGraph)
	djkShortestPathTree = {}
	sortestDistThusFarDict = defaultdict(infinet)
	closestAtomOrResThusFarDictAndParent = defaultdict(noneCallable)
	#
	###
	sortestDistThusFarDict[startNode] = 0
	closestAtomOrResThusFarDictAndParent[startNode] = None
	#
	h = []
	heapq.heappush(h, (0, startNode, None))
	#
	while len(djkShortestPathTree) < numberOfNodes:
		#distPriorityQue = [(dist, resOrAtom, closestAtomOrResThusFarDictAndParent[resOrAtom]) for resOrAtom, dist in sortestDistThusFarDict.items() if resOrAtom not in djkShortestPathTree]
		#if not distPriorityQue:break
		#dist, resOrAtom, predResOrAtom = min(distPriorityQue)
		if not h:break
		dist, resOrAtom, predResOrAtom = heapq.heappop(h)
		if resOrAtom in djkShortestPathTree:continue
		#
		djkShortestPathTree[resOrAtom] = predResOrAtom
		for interactingNode, interactions  in csuInteractionGraph.get(resOrAtom, {}).items():
			interactions = forceList(interactions)#to handel both residues and atoms - force list
			for interaction in interactions:
				if not interactionConstraintFilter or interactionConstraintFilter(interaction):
					#filter out spesific interactions based on filter function
					interactionWeight = interactionWeightFunc( interaction )
					if dist + interactionWeight < sortestDistThusFarDict[interactingNode]:
						sortestDistThusFarDict[interactingNode] = dist + interactionWeight
						closestAtomOrResThusFarDictAndParent[interactingNode] = resOrAtom
						#
						heapq.heappush(h, (dist + interactionWeight, interactingNode, resOrAtom))
						#
	return (djkShortestPathTree, sortestDistThusFarDict)

def getResInfoFromPyMolStr(pymolstr):
	backtickpos = pymolstr.find('`')
	ressecstart = pymolstr.rfind('/', 0, backtickpos) + 1
	ressecend = pymolstr.find('/', backtickpos)
	return pymolstr[ressecstart:backtickpos].title(), int(pymolstr[backtickpos+1:ressecend])

@namedtuple_decor
def Centrality(
	pymol, resnum, resname, region_name, ballesteros, centrality_score):
	pass

class CentralityCalculator(object):
	def __init__(self, resgraph, interactionWeightFunc=None, interactionConstraintFilter=None):
		self.resgraph = resgraph
		self.interactionWeightFunc = interactionWeightFunc
		self.interactionConstraintFilter = interactionConstraintFilter
		self.resset = self._reslist()
		self.rescount = len(self.resset)
	def _reslist(self):
		s = set()
		for k,d in self.resgraph.items():
			s.add(k)
			for k2, d2 in d.items():
				s.add(k2)
		return s

	def getDijkstraPredTreeAndShortestDist(self, startres):
		(djkShortestPathTree, sortestDistThusFarDict) = dijkstra(self.resgraph, startres, self.interactionWeightFunc, self.interactionConstraintFilter)
		return (djkShortestPathTree, sortestDistThusFarDict)

	def getResCentrality(self, startres):
		(djkShortestPathTree, sortestDistThusFarDict) = self.getDijkstraPredTreeAndShortestDist(startres)
		distsum = 0
		for res in self.resset:
			if res == startres: continue
			distsum += sortestDistThusFarDict[res]
		return float((self.rescount -1)) / float(distsum)


	def computeAllClosenessCentrality_pymolformat(self):
		closenesscentralitys = {}
		for res in self.resset:
			closenesscentralitys[res] = self.getResCentrality(res)
		return closenesscentralitys

	def computeAllClosenessCentrality(self):
		"""
		returns a dict maping res pymol str to the closness centrality

		>>> CCC=closeness_centrality.CentralityCalculator(data['ResidueData'])
		>>> CCC.computeAllClosenessCentrality()
		"""
		closenesscentralitys = []
		for respymolstr in self.resset:
			resname, resnum = getResInfoFromPyMolStr(respymolstr)
			closenesscentralitys.append( ((resname,resnum), self.getResCentrality(respymolstr)) )
		return closenesscentralitys
	
	def getAllCenralityTup(self, gpProIndexer):
		"""retruns a list of centrality tuples
		requires as input a instance of gpProIndexer to get the region_name
		and ballesteros 
		"""
		closenesscentralitys = []
		for respymolstr in self.resset:
			resname, resnum = getResInfoFromPyMolStr(respymolstr)
			centrality_score =  self.getResCentrality(respymolstr)
			region_name, ballesteros = gpProIndexer.translate(resnum)
			centralityTup = Centrality(
				respymolstr, resnum, resname, region_name, ballesteros, centrality_score)
			closenesscentralitys.append(centralityTup)
		return closenesscentralitys









#Centrality = namedtuple("Centrality", 'resnum resname region_name, ballesteros, centrality_score')
