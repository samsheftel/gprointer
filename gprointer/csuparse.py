__author__ = 'sam'
"""
"""


from Bio.PDB.PDBParser import PDBParser
import os
import subprocess
from collections import defaultdict, namedtuple

def _dictGetOrSetToAndReturn(dictionary, key, notFoundSetValue):
    """
    convience util function for dictioanrys, takes a dictatry a key and a valeu to set
        the key to if its not notfound. the importent point is the return value of
        the function is the object stored in the key 'key' so any changes made
        on the outpute will show up in the main dictioanary

    """
    keysValue = dictionary.get(key, None)
    if keysValue:
        return keysValue
    dictionary[key] = notFoundSetValue
    return dictionary.get(key)

#byts, linedictkey, typeCallable
CSUEXEC = "/root/FortranServices/CSU/a.out"
CSU_LINE_READER_CONFIG = (
(1, '', None),
(3, 'Residue1Name', str),
(5, 'Residue1Number', int),
(1, '', None),
(1, '', None),
(1, '', None),
(1, 'Residue1Chain', str),
(1, '', None),
(3, 'Atom1Name', str),
(6, 'AccessibleSurface', float),
(6, 'FreeAccessibleSurface', float),
(1, '', None),
(3, 'Residue2Name', str),
(5, 'Residue2Number', int),
(1, '', None),
(1, '', None),
(1, '', None),
(1, 'Residue2Chain', str),
(1, '', None),
(3, 'Atom2Name', str),
(5, 'Distance', float),
(5, 'ContactSurface', float),
(2, 'Atom1Type', int),
(2, 'Atom2Type', int),
(1, '', None)
)
CSU_LINE_LENGTH_READER = sum([byts for byts, keyname, typecallable in CSU_LINE_READER_CONFIG])
Interaction = namedtuple('Interaction', 'atom1 Atom1Type atom2 Atom2Type Distance AccessibleSurface ContactSurface FreeAccessibleSurface')
#

def getPdbPathAndProteinName(pdbFileObjOrPath):
    """
    takes either a pdb file path as a string or a file obj, returns the path of the pdb
        and the name of the Structure

    >>> getPdbPathAndProteinName(PDBFILE1)
    ('/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/2RH1.pdb', '2RH1')
    >>> testopenfile = open(PDBFILE1,'r')
    >>> getPdbPathAndProteinName(testopenfile)
    ('/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/2RH1.pdb', '2RH1')
    >>> testopenfile.close()
    """
    if isinstance(pdbFileObjOrPath, file):
        pdbFileObjOrPath = pdbFileObjOrPath.name
    absolutePdbFilePath = os.path.abspath(pdbFileObjOrPath)
    pdbFileDir, pdbFileName = os.path.split(absolutePdbFilePath)
    proteinName, pdbFileExtention = os.path.splitext(pdbFileName)
    return (absolutePdbFilePath, proteinName)


def parserPdb(pdbFileObjOrPath):
    """
    returns the Biopython pdb object for the pdb file | file obj passed in

    >>> parserPdb(PDBFILE1)
    <Structure id=2RH1>
    >>> testopenfile = open(PDBFILE1,'r')
    >>> parserPdb(testopenfile)
    <Structure id=2RH1>
    >>> testopenfile.close()
    """
    parser = PDBParser()
    absolutePdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
    return parser.get_structure(proteinName, absolutePdbFilePath)


def getChainListFromPdbObj(PdbObj):
    """
    takes a biopython pdb obj and returns a Iterater of the chain names (ie id) in the object

    >>> pdb = parserPdb(PDBFILE1)
    >>> chainIter = getChainListFromPdbObj(pdb)
    >>> list(chainIter)
    ['A']
    """
    for chain in PdbObj.get_chains():
        yield chain.get_id()

def csuChainSetsCombinations(chains):
    """
    >>> from Bio.PDB.PDBParser import PDBParser
    >>> parser = PDBParser()
    >>> pdb = parser.get_structure(PDBNAME1, PDBFILE1)
    >>> chains = getChainListFromPdbObj(pdb)
    >>> csuChainSets = csuChainSetsCombinations(chains)
    >>> list(csuChainSets)
    [('A', 'A')]
    """
    chains = list(chains)
    numberOfChains = len(chains)
    for chain1 in range(numberOfChains):
        for chain2 in range(chain1, numberOfChains):
            yield (chains[chain1], chains[chain2])


def runCsu(csuExec, pdbFilePath, chain1, chain2):
    """

    Returs the file Like Csu Subprosses
    >>> pdb = parserPdb(PDBFILE1)
    >>> chains = getChainListFromPdbObj(pdb)
    >>> csuChainSets = csuChainSetsCombinations(chains)
    >>> chain1, chain2 = csuChainSets.next()
    >>> csuProcess = runCsu(CSUEXEC, PDBFILE1, chain1, chain2)
    >>> csuProcess.stdout.read(60)
    ' ASP   29   A N    39.0 120.8 ASP   29   A CA   1.5 54.4 3 7'

    """
    return subprocess.Popen([csuExec, pdbFilePath, "77", chain1, chain2], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)


def _isAdjsentInteractions_screener(csuLineDict):
    """
    a filter or screaner function used by parsesCsuOutpute to remove atomic interaction between backbon atoms or atoms in same residues

    >>> adjcentInteractionCsuLine = {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 1.5, 'AccessibleSurface': 39.0, 'ContactSurface': 54.399999999999999, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 29, 'Atom2Name': 'CA', 'Atom1Type': 3, 'Atom2Type': 7, 'Residue2Name': 'ASP', 'Residue2Chain': 'A'}
    >>> nonAdjcentInteractionCsuLine = {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 3.0, 'AccessibleSurface': 39.0, 'ContactSurface': 2.7999999999999998, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 30, 'Atom2Name': 'N', 'Atom1Type': 3, 'Atom2Type': 3, 'Residue2Name': 'GLU', 'Residue2Chain': 'A'}
    >>> _isAdjsentInteractions_screener(adjcentInteractionCsuLine)
    False
    >>> _isAdjsentInteractions_screener(nonAdjcentInteractionCsuLine)
    True

    """
    sameChain = csuLineDict['Residue1Chain'] == csuLineDict['Residue2Chain']
    sameResidue = csuLineDict['Residue1Number'] == csuLineDict['Residue2Number']
    if sameChain and sameResidue:
        return False
    if sameChain and abs(csuLineDict['Residue2Number'] - csuLineDict['Residue1Number']) == 1:
        #if naubor residues
        if csuLineDict['Residue1Number'] < csuLineDict['Residue2Number']:
            lowerNumberedResiduesAtom, higherNumberedResiduesAtom  = csuLineDict['Atom1Name'], csuLineDict['Atom2Name']
        else:
            lowerNumberedResiduesAtom, higherNumberedResiduesAtom  = csuLineDict['Atom2Name'], csuLineDict['Atom1Name']
        if lowerNumberedResiduesAtom == 'N' and higherNumberedResiduesAtom == 'C':
            #back bone connected residues#CHECKS
            return False
    return True


def parsesCsuOutpute(csuStdOutObj, excludeAdjsentInteractions=True, screenerFunction=None):
    """
    takes a file like stdout obj with the csu stdout and returns a iterator of parsed csu line dict (csuLineDict)

    screenerFunction is optional callable which expects the pared line dict as its only arg
        and returs a bool T/F indicating weather the line dict should be include in output

    >>> pdb = parserPdb(PDBFILE1)
    >>> chains = getChainListFromPdbObj(pdb)
    >>> csuChainSets = csuChainSetsCombinations(chains)
    >>> chain1, chain2 = csuChainSets.next()
    >>> csuProcess = runCsu(CSUEXEC, PDBFILE1, chain1, chain2)
    >>> parsedCsuLines = parsesCsuOutpute(csuProcess.stdout, False)
    >>> parsedCsuLines.next()
    {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 1.5, 'AccessibleSurface': 39.0, 'ContactSurface': 54.399999999999999, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 29, 'Atom2Name': 'CA', 'Atom1Type': 3, 'Atom2Type': 7, 'Residue2Name': 'ASP', 'Residue2Chain': 'A'}
    >>> csuProcess = runCsu(CSUEXEC, PDBFILE1, chain1, chain2)
    >>> parsedCsuLines = parsesCsuOutpute(csuProcess.stdout, True)
    >>> parsedCsuLines.next()
    {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 3.0, 'AccessibleSurface': 39.0, 'ContactSurface': 2.7999999999999998, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 30, 'Atom2Name': 'N', 'Atom1Type': 3, 'Atom2Type': 3, 'Residue2Name': 'GLU', 'Residue2Chain': 'A'}
    """
    continueReading = True
    while continueReading:
        linedata = {}
        for readlen, dictkey, typeconverter in CSU_LINE_READER_CONFIG:
            rawstr = csuStdOutObj.read(readlen)
            if len(rawstr) < readlen:
                #end of file reatched when len(rawstr) < readlen
                continueReading = False
                break
            if dictkey:
                linedata[dictkey] = typeconverter(rawstr.strip())
        if not linedata: continue
        if excludeAdjsentInteractions and not(_isAdjsentInteractions_screener(linedata)):
            #jump to next iteration of while loop if the interation is between adjacent res or atoms
            continue
        if not screenerFunction or (screenerFunction and screenerFunction(linedata)):
            yield linedata

def pymolformat(structure, chain, residueName, residueNumber, atomName='', atomNumber=''):
    """
    returns the pymol formated selection string for residue, or atom, or chain

    atomNumber argumnet never used

    >>> pymolformat('2RH1', 'A', 'GLU', 30, 'N')
    '/2RH1//A/GLU`30/N'
    """
    return '/%s//%s/%s`%s/%s' % (structure, chain, residueName, str(residueNumber), atomName)

def getPyMolAtomStringsFromCsuLine(csuLineDict, proteinName=''):
    """
    from a csu line dict returns the pymole selection string format for the two *Atoms* in interaction
        the return is a 2-tuple of pymol selection strings

    >>> csuLineData = {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 3.0, 'AccessibleSurface': 39.0, 'ContactSurface': 2.7999999999999998, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 30, 'Atom2Name': 'N', 'Atom1Type': 3, 'Atom2Type': 3, 'Residue2Name': 'GLU', 'Residue2Chain': 'A'}
    >>> getPyMolAtomStringsFromCsuLine(csuLineData, '2RH1')
    ('/2RH1//A/ASP`29/N', '/2RH1//A/GLU`30/N')
    >>> getPyMolAtomStringsFromCsuLine(csuLineData)
    ('///A/ASP`29/N', '///A/GLU`30/N')
    """
    return (
    pymolformat(proteinName, csuLineDict['Residue1Chain'], csuLineDict['Residue1Name'], csuLineDict['Residue1Number'], csuLineDict['Atom1Name']),
    pymolformat(proteinName, csuLineDict['Residue2Chain'], csuLineDict['Residue2Name'], csuLineDict['Residue2Number'], csuLineDict['Atom2Name'])
    )

def getPyMolResidueStringsFromCsuLine(csuLineDict, proteinName=''):
    """
    from a csu line dict returns the pymole selection string format for the two *Residues* in interaction
        the return is a 2-tuple of pymol selection strings

    >>> csuLineData = {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 3.0, 'AccessibleSurface': 39.0, 'ContactSurface': 2.7999999999999998, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 30, 'Atom2Name': 'N', 'Atom1Type': 3, 'Atom2Type': 3, 'Residue2Name': 'GLU', 'Residue2Chain': 'A'}
    >>> getPyMolResidueStringsFromCsuLine(csuLineData, '2RH1')
    ('/2RH1//A/ASP`29/', '/2RH1//A/GLU`30/')
    >>> getPyMolResidueStringsFromCsuLine(csuLineData)
    ('///A/ASP`29/', '///A/GLU`30/')
    """
    return (
    pymolformat(proteinName, csuLineDict['Residue1Chain'], csuLineDict['Residue1Name'], csuLineDict['Residue1Number']),
    pymolformat(proteinName, csuLineDict['Residue2Chain'], csuLineDict['Residue2Name'], csuLineDict['Residue2Number'])
    )



def CSUInteractionStream(pdbFileObjOrPath):
    """
    CSUInteractionStream(<String-Path|FileObj>) -> iter(csuLineDict)

    List of all interaction ie csuLineDict pared out from running
        csu on the input pdb

    >>> csuLineDictIter = CSUInteractionStream(PDBFILE1)
    >>> csuLineDictIter.next()
    {'Atom1Name': 'N', 'Residue1Name': 'ASP', 'Residue1Number': 29, 'Residue1Chain': 'A', 'Distance': 3.0, 'AccessibleSurface': 39.0, 'ContactSurface': 2.7999999999999998, 'FreeAccessibleSurface': 120.8, 'Residue2Number': 30, 'Atom2Name': 'N', 'Atom1Type': 3, 'Atom2Type': 3, 'Residue2Name': 'GLU', 'Residue2Chain': 'A'}
    """
    pdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
    pdb = parserPdb(pdbFilePath)
    csuChainSets = csuChainSetsCombinations(getChainListFromPdbObj(pdb))
    for chain1, chain2 in csuChainSets:
        csuProcess = runCsu(CSUEXEC, pdbFilePath, chain1, chain2)
        parsedCsuLines = parsesCsuOutpute(csuProcess.stdout, True)
        for parsedCsuLine in parsedCsuLines:
            yield parsedCsuLine


def CSUInteractionData(pdbFileObjOrPath):
    """
    CSUInteractionData(<String-Path|FileObj>) -> (dict<string, dict<string, csuLineDict>>, dict<string, dict<string, list<csuLineDict>>>)

    Main CSU function, takes a pdb file path | file obj as its input and returs two dict with all itneraction info
        the first dict is the atom interaction dict and is of type dict<string, dict<string, csuLineDict>>.
        the second dict is the residue interation dict and is of type dict<string, dict<string, list<csuLineDict>>>

    >>> csuAtomData, csuResidueData = CSUInteractionData(PDBFILE1)
    >>> csuAtomData['/2RH1//A/SO4`404/O3']['/2RH1//A/ASN`1132/OD1']
    {'Atom1Name': 'O3', 'Residue1Name': 'SO4', 'Residue1Number': 404, 'Residue1Chain': 'A', 'Distance': 3.1000000000000001, 'AccessibleSurface': 11.6, 'ContactSurface': 21.300000000000001, 'FreeAccessibleSurface': 105.7, 'Residue2Number': 1132, 'Atom2Name': 'OD1', 'Atom1Type': 1, 'Atom2Type': 2, 'Residue2Name': 'ASN', 'Residue2Chain': 'A'}
    >>> csuResidueData['/2RH1//A/ALA`76/']['/2RH1//A/VAL`54/']
    [{'Atom1Name': 'CB', 'Residue1Name': 'ALA', 'Residue1Number': 76, 'Residue1Chain': 'A', 'Distance': 4.0999999999999996, 'AccessibleSurface': 0.69999999999999996, 'ContactSurface': 19.300000000000001, 'FreeAccessibleSurface': 136.80000000000001, 'Residue2Number': 54, 'Atom2Name': 'CG1', 'Atom1Type': 4, 'Atom2Type': 4, 'Residue2Name': 'VAL', 'Residue2Chain': 'A'}, {'Atom1Name': 'CB', 'Residue1Name': 'ALA', 'Residue1Number': 76, 'Residue1Chain': 'A', 'Distance': 5.2000000000000002, 'AccessibleSurface': 0.69999999999999996, 'ContactSurface': 0.20000000000000001, 'FreeAccessibleSurface': 136.80000000000001, 'Residue2Number': 54, 'Atom2Name': 'CG2', 'Atom1Type': 4, 'Atom2Type': 4, 'Residue2Name': 'VAL', 'Residue2Chain': 'A'}]
    """
    csuAtomData, csuResidueData = {}, {}
    pdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
    pdb = parserPdb(pdbFilePath)
    csuChainSets = csuChainSetsCombinations(getChainListFromPdbObj(pdb))
    for chain1, chain2 in csuChainSets:
        csuProcess = runCsu(CSUEXEC, pdbFilePath, chain1, chain2)
        parsedCsuLines = parsesCsuOutpute(csuProcess.stdout, True)
        for parsedCsuLine in parsedCsuLines:
            #atom
            pymolSelectionStringAtom1, pymolSelectionStringAtom2 = getPyMolAtomStringsFromCsuLine(parsedCsuLine, proteinName=proteinName)
            _dictGetOrSetToAndReturn( _dictGetOrSetToAndReturn(csuAtomData, pymolSelectionStringAtom1, {}), pymolSelectionStringAtom2, parsedCsuLine)
            #residue
            pymolSelectionStringResidue1, pymolSelectionStringResidue2 = getPyMolResidueStringsFromCsuLine(parsedCsuLine, proteinName=proteinName)
            _dictGetOrSetToAndReturn( _dictGetOrSetToAndReturn(csuResidueData, pymolSelectionStringResidue1, {}),  pymolSelectionStringResidue2, []).append(parsedCsuLine)
    return (csuAtomData, csuResidueData)


def CSUInteractionData2(pdbFileObjOrPath):
    """
    BETA
    
    CSUInteractionData(<String-Path|FileObj>) -> (dict<string, dict<string, csuLineDict>>, dict<string, dict<string, list<csuLineDict>>>)

    Main CSU function, takes a pdb file path | file obj as its input and returs two dict with all itneraction info
        the first dict is the atom interaction dict and is of type dict<string, dict<string, csuLineDict>>.
        the second dict is the residue interation dict and is of type dict<string, dict<string, list<csuLineDict>>>

    >>> csuAtomData, csuResidueData = CSUInteractionData(PDBFILE1)
    >>> csuAtomData['/2RH1//A/SO4`404/O3']['/2RH1//A/ASN`1132/OD1']
    {'Atom1Name': 'O3', 'Residue1Name': 'SO4', 'Residue1Number': 404, 'Residue1Chain': 'A', 'Distance': 3.1000000000000001, 'AccessibleSurface': 11.6, 'ContactSurface': 21.300000000000001, 'FreeAccessibleSurface': 105.7, 'Residue2Number': 1132, 'Atom2Name': 'OD1', 'Atom1Type': 1, 'Atom2Type': 2, 'Residue2Name': 'ASN', 'Residue2Chain': 'A'}
    >>> csuResidueData['/2RH1//A/ALA`76/']['/2RH1//A/VAL`54/']
    [{'Atom1Name': 'CB', 'Residue1Name': 'ALA', 'Residue1Number': 76, 'Residue1Chain': 'A', 'Distance': 4.0999999999999996, 'AccessibleSurface': 0.69999999999999996, 'ContactSurface': 19.300000000000001, 'FreeAccessibleSurface': 136.80000000000001, 'Residue2Number': 54, 'Atom2Name': 'CG1', 'Atom1Type': 4, 'Atom2Type': 4, 'Residue2Name': 'VAL', 'Residue2Chain': 'A'}, {'Atom1Name': 'CB', 'Residue1Name': 'ALA', 'Residue1Number': 76, 'Residue1Chain': 'A', 'Distance': 5.2000000000000002, 'AccessibleSurface': 0.69999999999999996, 'ContactSurface': 0.20000000000000001, 'FreeAccessibleSurface': 136.80000000000001, 'Residue2Number': 54, 'Atom2Name': 'CG2', 'Atom1Type': 4, 'Atom2Type': 4, 'Residue2Name': 'VAL', 'Residue2Chain': 'A'}]
    """
    csuAtomData, csuResidueData = {}, {}
    pdbFilePath, proteinName = getPdbPathAndProteinName(pdbFileObjOrPath)
    pdb = parserPdb(pdbFilePath)
    csuChainSets = csuChainSetsCombinations(getChainListFromPdbObj(pdb))
    for chain1, chain2 in csuChainSets:
        csuProcess = runCsu(CSUEXEC, pdbFilePath, chain1, chain2)
        parsedCsuLines = parsesCsuOutpute(csuProcess.stdout, True)
        for parsedCsuLine in parsedCsuLines:
            #atom
            pymolSelectionStringAtom1, pymolSelectionStringAtom2 = getPyMolAtomStringsFromCsuLine(parsedCsuLine, proteinName=proteinName)
            _dictGetOrSetToAndReturn( _dictGetOrSetToAndReturn(csuAtomData, pymolSelectionStringAtom1, {}), pymolSelectionStringAtom2, parsedCsuLine)
            #residue
            pymolSelectionStringResidue1, pymolSelectionStringResidue2 = getPyMolResidueStringsFromCsuLine(parsedCsuLine, proteinName=proteinName)
            res1InterList = _dictGetOrSetToAndReturn(csuResidueData, pymolSelectionStringResidue1, [])
            res1InterList.append(parsedCsuLine)
            res2InterList = _dictGetOrSetToAndReturn(csuResidueData, pymolSelectionStringResidue2, [])
            res2InterList.append(parsedCsuLine)
    return (csuAtomData, csuResidueData)



def _test():
    import doctest
    import os
    CSUEXEC = "/root/FortranServices/CSU/a.out"
    CSUEXECNAME = os.path.split(CSUEXEC)[1]
    CSUDIR = os.path.dirname(CSUEXEC)
    PDBFILE1 = "/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/2RH1.pdb"
    PDBNAME1 = "2RH1"
    PDBFILE2 = "/mnt/hgfs/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/DevPlayground/ProrintTestPlayground/ParserAndRunerReDo/1YTB.pdb"
    PDBNAME2 = "1YTB"

    doctest.testmod(verbose=True, report=True, extraglobs=locals())

if __name__ == "__main__":
    _test()