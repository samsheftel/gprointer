__author__ = 'Samuel Sheftel'
"""
secondary_structure.py is a wrapper around PROSS.py that determines the most 
probable 2nd structure of a pdb. 

see http://roselab.jhu.edu/utils/fg_defs.html

"""

import PROSS
import collections
try:
	from collections import OrderedDict
except ImportError:
	from ordereddict import OrderedDict


SECONDARY_STRUCTURE_CODE_MAP = {
	'H': 'alphahelix',
	'E': 'betastrand',
	'T': 'betaturn',
	'P': 'pconf',
	'C': 'coil'
}
AMINO_ACID_ABV = {'ILE': 'I', 'GLN': 'Q', 'XLE': 'J', 'GLX': 'Z', 'GLY': 'G', 'GLU': 'E', 'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'LYS': 'K', 'PRO': 'P', 'ASX': 'B', 'SEC': 'U', 'ASN': 'N', 'VAL': 'V', 'THR': 'T', 'HIS': 'H', 'TRP': 'W', 'PHE': 'F', 'ALA': 'A', 'MET': 'M', 'PYL': 'O', 'LEU': 'L', 'ARG': 'R', 'TYR': 'Y'}
ProssData = collections.namedtuple('ProssData', 'res_idx, res_name, classification, sec_struct_code, ms_helix_strand_code, phi, psi, ome, chi1, chi2, chi3, chi4')


def get_pross_data(pdb_path):
	"""
	get_pross_data('pdb_file_path') -> OrderedDict<'res_id_str', ProssData>
	res_id_str is a string of the form 'chain_resabv_resnum' like 'A_VAL_33'
	ProssData is a tuple  with fields of 
		res_idx, res_name, sec_struct, ms_helix_strand_code, phi, psi, ome, chi1, chi2, chi3, chi4
	"""
	pross_data = OrderedDict()
	mol = PROSS.read_pdb(pdb_path)
	mol.delete_hetero()
	mol.delete_hetero()
	for chain in mol:
		if chain.type() != 'protein':continue
		chain.gaps()
		phi, psi, ome, chi1, chi2, chi3, chi4 = chain.torsions()
		phi, psi, ome, ss = chain.pross(phi, psi, ome)
		for pos, res in enumerate(chain.elements):
			ms = PROSS.res_rc(phi[pos], psi[pos],mcodes='fgmeso')
			res_id_str = '%s_%s_%s' % (chain.name, res.name, res.idx)
			if pross_data.has_key(res_id_str):
				raise KeyError('duplicate residue key - %s' % res_id_str)
			residue_pross_data = ProssData(
				res.idx, res.name, SECONDARY_STRUCTURE_CODE_MAP[ss[pos]], \
				ss[pos], ms, phi[pos], psi[pos], ome[pos],\
				chi1[pos], chi2[pos], chi3[pos], chi4[pos]
			)
			pross_data[res_id_str] = residue_pross_data
	return pross_data


class SecStructure(object):

	def __init__(self, pdb_path, max_consec_non_alpha=3, larger_then=13):
		self.pdb_path = pdb_path
		self.data = get_pross_data(pdb_path)
		self.max_consec_non_alpha = max_consec_non_alpha
		self.larger_then = larger_then
		self.alphahelixes = self.get_alphahelixes(
			self.max_consec_non_alpha, self.larger_then
		)

	@staticmethod
	def _parse_res_id_str(res_id):
		"""privite metiod for converting res_id to tuple of form:
			(chain, res_abrev, res_num)
		
		res_id are a string for uneque id of a residue for use in dicts
		its format is 'chain_resabrev_resnum'.

		>>> SecStructure._parse_res_id_str('A_VAL_33')
		('A', 'VAL', 33)

		"""
		chain, res_abv, res_num = res_id.split('_')
		res_num = int(res_num)
		return (chain, res_abv, res_num)

	def get_alphahelixes(self, max_consec_non_alpha, larger_then):
		helix_seqs = []
		helix_start, helix_end = None, None
		running_seq = []
		consecutive_non_alpha = 0
		for res_id, res_pross_data in self.data.iteritems():
			if res_pross_data.classification == 'alphahelix':
				if helix_start == None:
					helix_start = helix_end = res_id
					running_seq.append(res_id)
				else:
					helix_end = res_id
					running_seq.append(res_id)
			else:
				consecutive_non_alpha += 1
			if consecutive_non_alpha >= max_consec_non_alpha and helix_end:
				_, __, start_res_num = self._parse_res_id_str(helix_start)
				_, __, end_res_num = self._parse_res_id_str(helix_end)
				if end_res_num - start_res_num >= larger_then:
					helix_seqs.append( running_seq )
				helix_start, helix_end = None, None
				consecutive_non_alpha = 0
				running_seq = []
		return helix_seqs

	def get_alphahelix_ranges(self):
		helix_ranges = []
		for helix_seq in self.alphahelixes:
			helix_ranges.append( (helix_seq[0], helix_seq[-1]) )
		return helix_ranges

	def get_alphahelixes_primary_seqs(self):
		helix_primary_seqs = []
		for helix_seq in self.alphahelixes:
			primary_seq = []
			for res_id_str in helix_seq:
				chain, res_abv, res_num = self._parse_res_id_str(res_id_str)
				symbol = AMINO_ACID_ABV.get(res_abv)
				primary_seq.append(symbol)
			helix_primary_seqs.append(primary_seq)
		return helix_primary_seqs

	def _get_search_patterns(self, helix_range):
		"""gets search pattern from helix_range"""
		schain, sres_abv, sres_num = self._parse_res_id_str(helix_range[0])
		echain, eres_abv, eres_num = self._parse_res_id_str(helix_range[1])
		start_pat = '{0} {1}{2:>4}'.format(sres_abv, schain, sres_num)
		end_pat = '{0} {1}{2:>4}'.format(eres_abv, echain, eres_num)
		return (start_pat, end_pat)

	def get_alphahelix_pdb_lines(self):
		#print('HIiiiii')
		pdb_lines = []
		helix_ranges = self.get_alphahelix_ranges()
		helix_count = len(helix_ranges)
		range_index = 0# the index of the helix range currently in use for search
		start_pat, end_pat = self._get_search_patterns(helix_ranges[range_index]) 
		current_helix_lines = []
		with open(self.pdb_path, 'r') as f:
			pdb_file_lines = f.readlines()
		for pdb_line in pdb_file_lines:
			if range_index >= helix_count: break
			if current_helix_lines:# true when found start but not found end yet
				current_helix_lines.append(pdb_line)
				if end_pat in pdb_line:#hit end of helix
					pdb_lines.append(current_helix_lines)
					current_helix_lines = []
					range_index += 1
					if range_index >= helix_count: break
					start_pat, end_pat = self._get_search_patterns(helix_ranges[range_index])
			else:
				if start_pat in pdb_line:
					current_helix_lines.append(pdb_line)
		return pdb_lines

	def write_all_alphahelix_to_file(self, new_file_path):
		alphahelix_pdb_lines = self.get_alphahelix_pdb_lines()
		with open(new_file_path, 'w') as f:
			for helix in alphahelix_pdb_lines:
				for pdb_line in helix:
					f.write(pdb_line)
			f.write('END\n')






