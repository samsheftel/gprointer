

#res_centrality
from pdbutils import loadjson, savetojson, savetocsv
from closeness_centrality import CentralityCalculator, Centrality
from gproindexer import GProIndexer
from operator import attrgetter

FILE_DIR = """/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/CostanziEmail/redatascripts/"""

CSU_DATA_JSON = "{FILE_DIR}/2RH1_edited_csu_data.json".format(FILE_DIR=FILE_DIR)
CENTRALITY_DATA_CSV = "{FILE_DIR}/2RH1_edited_centrality_data.csv".format(FILE_DIR=FILE_DIR)
CENTRALITY_DATA_JSON = "{FILE_DIR}/2RH1_edited_centrality_data.json".format(FILE_DIR=FILE_DIR)

#CSU_DATA_JSON = "{FILE_DIR}/3P0G_edited_csu_data.json".format(FILE_DIR=FILE_DIR)
#CENTRALITY_DATA_CSV = "{FILE_DIR}/3P0G_edited_centrality_data.csv".format(FILE_DIR=FILE_DIR)
#CENTRALITY_DATA_JSON = "{FILE_DIR}/3P0G_edited_centrality_data.json".format(FILE_DIR=FILE_DIR)

csu_data = loadjson(CSU_DATA_JSON)
residue_graph = csu_data.get("ResidueData")
centralityCalculator = CentralityCalculator(residue_graph)
gpProIndexer = GProIndexer(
	"""Nt = 1 - 28
	TM1 = 29 - 60 50
	IL1 =  61 - 66
	TM2 = 67 - 96 79
	EL1 = 97 - 101
	TM3 = 102 - 136 131
	IL2 = 137 - 146
	TM4 = 147 - 170 158
	EL2 = 171 - 196
	TM5 = 197 - 230 211
	IL3 = 231 - 266
	TM6 = 267 - 298 288
	EL3 = 299 - 304
	TM7 = 305 - 328 323
	H8 = 329 - 341
	Ct = 342 - 413"""
)



Centrality_Data = centralityCalculator.getAllCenralityTup(gpProIndexer)
#
#Centrality_Data = []
#for (resname, resnum), centrality_score in centralityCalculator.computeAllClosenessCentrality():
#	region_name, ballesteros = gpProIndexer.translate(resnum)
	#resnum resname region_name, ballesteros, centrality_score
#	Centrality_Data.append( Centrality( resnum, resname, region_name, ballesteros, centrality_score) )
#Centrality_Data = sorted(Centrality_Data, key=attrgetter('centrality_score'), reverse=True)

Centrality_Data = sorted(Centrality_Data, key=attrgetter('resnum'), reverse=False)

savetocsv(Centrality._fields, Centrality_Data, CENTRALITY_DATA_CSV)

Centrality_Data_Dicts = [centrality._asdict() for centrality in Centrality_Data]

savetojson(Centrality_Data_Dicts, CENTRALITY_DATA_JSON)

