__author__ = 'Samuel Sheftel'
"""
general utils for pdb like fetch 

also see: https://code.google.com/p/pdb-tools/
"""
import urllib
import json
import shutil
import os
import csv
import re
from collections import namedtuple
RCSB_PDB_URL = 'http://www.rcsb.org/pdb/files/%s.pdb'
UNIPROT_PATTERN = re.compile('\s(P0\d+)\s')
PYMOLSELECTIONPATTERN = re.compile('/([\w\d]+)//(\w)/((\w+)`(\d+))(/(\w+)?)?')

def fetch_pdb(pdb_id):
	"""gets pdb file obj from pdb_id
	:pdb id: the protien pdb id code (NO .pdb at end)
	"""
	pdb_url = RCSB_PDB_URL % pdb_id
	return urllib.urlopen(pdb_url)

def download_pdb(pdb_id, directory=None, name=None):
	"""downlaods a pdb from RCSB and save it to a local file
	:pdb id: the protien pdb id code (NO .pdb at end)
	:param directory: path to the folder to save the pdb in defaule current dir
	:param name: the name of the file the pdb is saved to defult id pdb_id.pdb

	>>> download_pdb('2RH1', '/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets')
	'/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/2RH1.pdb'

	"""
	directory = directory or os.path.abspath(os.curdir)
	name = name or pdb_id + '.pdb'
	save_path = os.path.join(directory, name)
	pdb_file = fetch_pdb(pdb_id)
	with open(save_path, 'w') as save_to:
		shutil.copyfileobj(pdb_file, save_to)
	return save_path


def get_uniprot_id(pdb_id):
	pdb_file = fetch_pdb(pdb_id)
	P0_ids = []
	for line in pdb_file:
		if 'DBREF' in line and 'P0' in line:
			P0_Start = line.find(' P0')+1
			P0_End = line.find(' ', P0_Start)
			P0_ids.append( line[P0_Start:P0_End] )
	return P0_ids


def loadjson(fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'r') as f:
		data = json.load(f)
	return data

def savetojson(obj, fpath, pretty_print=True):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'w') as f:

		if pretty_print:
			json.dump(obj, f, indent=4)
		else:
			json.dump(obj, f, indent=4)
	return fpath

def savetocsv(headerrow, rows, fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'w') as f:
		csv_writer = csv.writer(f, dialect='excel')
		csv_writer.writerow(headerrow)
		for row in rows:
			csv_writer.writerow(row)
	return fpath


def parse_pymol_format(pymol_format_str):
	""" converts pdb format strig (res or atom spesification) to tuple of form
		struct, chain, resname, resnum, atomname

	>>> parse_pymol_format("/2RH1_edited//A/GLU`187/C")
	('2RH1_edited', 'A', 'GLU', 187, 'C')
	>>> parse_pymol_format("/2RH1_edited//A/GLU`187")
	('2RH1_edited', 'A', 'GLU', 187, None)

	"""
	matchobj = PYMOLSELECTIONPATTERN.match(pymol_format_str.strip())
	struct, chain, res, resname, resnum, slash, atomname = matchobj.groups()
	resnum = int(resnum) if resnum.isdigit() else resnum
	return struct, chain, resname, resnum, atomname

def readfile(fpath):
	"""gets the text of a file"""
	with open(fpath, 'r') as f:
		filestr = f.read()
	return filestr

def makeNewFileNameFromOld(file_path, append_str, new_extension=None):
	"""takes a file_path with name and modifyes the pre extetion part

	>>> file_path = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/utils.py"
	>>> makeNewFileNameFromOld(file_path, "jim_jim")
	'/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/gprointer/data_scripts/utils_jim_jim.py'
	"""
	file_path = os.path.abspath(file_path)
	append_str = append_str if append_str.startswith('_') else '_'+ append_str
	if not os.path.exists(file_path) or not os.path.isfile(file_path):
		raise NameError("file path given not exist")
	dir_path, file_name = os.path.split(file_path)
	name, extension = os.path.splitext(file_name)
	extension = new_extension or extension
	extension = extension if extension[0] == '.' else '.' + extension
	new_file_name = name + append_str + extension
	new_path = os.path.join(dir_path, new_file_name)
	return new_path

def namedtuple_decor(func):
	"""decorator for creating a named tuple

	>>> @namedtuple_decor
	... def Person(first_name, last_name, age): pass
	>>> Person("Sam", "Sheftel", 25)
	Person(first_name='Sam', last_name='Sheftel', age=25)
	"""
	return namedtuple(func.__name__, func.__code__.co_varnames)





