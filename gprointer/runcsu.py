#!/usr/bin/env python
"""

python runcsu.py 
"""


import sys 
from csuparse import CSUInteractionData
from pdbutils import makeNewFileNameFromOld
import os
import json


def main():
	arg_count = len(sys.argv)
	if arg_count < 2:
		raise ValueError('not enough args')
	pdb_file = sys.argv[1]
	if arg_count > 2:
		csu_output_file = sys.argv[2]
	else:
		csu_output_file = makeNewFileNameFromOld(pdb_file, 'csu_data', '.json')

	AtomData, ResidueData = CSUInteractionData(pdb_file)
	data = dict(AtomData=AtomData, ResidueData=ResidueData)

	with open(csu_output_file, 'w') as f:
		json.dump(data, f, indent=4)
	return os.path.abspath(csu_output_file)


if __name__ == '__main__':
	main()