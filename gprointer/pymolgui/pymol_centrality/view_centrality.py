"""
view_centrality.py
"""
from __future__ import with_statement
PLUGINNAME = "view_centrality"
############################samol###############################################
class MockPymolCmd(object):
	def __init__(self):
		pass
	def color(self, clr, selstr):
		cmdstr = 'cmd.color(%s, %s)' % (repr(clr), repr(selstr))
		print(cmdstr)
		return cmdstr
	def show(self, shape, selstr):
		cmdstr = 'cmd.show(%s, %s)' % (repr(shape), repr(selstr))
		print(cmdstr)
		return cmdstr
	def select(self, name, selstr):
		cmdstr = 'cmd.select(%s, %s)' % (repr(name), repr(selstr))
		print(cmdstr)
		return cmdstr
	def hide(self, shapes, selstr):
		cmdstr = 'cmd.hide(%s, %s)' % (repr(shapes), repr(selstr))
		print(cmdstr)
		return cmdstr
class MockPymolUtil(object):
	def __init__(self):
		pass
	def cba(self, colornum, selstr, **kwargs):
		_self = kwargs.get('_self', None)
		_self_str = '_self=cmd'
		cmdstr = 'util.cba(%s, %s, %s)' % (repr(colornum), repr(selstr), _self_str)
		print(cmdstr)
		return cmdstr

try:
	from pymol import cmd
	#from pymol import _cmd
	from pymol import util
	INSIDE_PYMOL = True
except ImportError:
	INSIDE_PYMOL = False
	cmd = MockPymolCmd()
	util = MockPymolUtil()

def forceList(arg):
	if not isinstance(arg, (list, set)):
		arg = [arg]
	return arg

def resetColorBugFix(selstr):
	res_list = selstr.replace('i. ', '').strip().split('+')
	fixed_res_sel_str = 'resi ' + ','.join([str(r) for r in res_list])
	#print("\n\n\n\tresetColorBugFix selstr = "+fixed_res_sel_str+"\n\n")
	#util.cba(33, fixed_res_sel_str) 
	#util.cba(33)
	cmd.do("util.cba(33, '"+fixed_res_sel_str+"')")


def resetColor(selstr):
	""""""
	#print("\n\nselstr in restColor="+selstr+"\n\n")
	#util.cba(33, selstr, _self=cmd)
	#util.cba(33)
	#eval('util.cba(33)')
	#eval('util.cba(33,"2RH1_eo")')#worked
	#print('\n\n'+selstr+'\n\n')
	#util.cba(33, selstr)
	util.cba(33, selstr, _self=cmd)#origanal

def resetShape(selstr):
	cmd.hide("everything", selstr)
	cmd.show("lines", selstr)
def selResNums(resnumlist):
	resnumlist = forceList(resnumlist)
	reses = '+'.join([str(resnum) for resnum in resnumlist])
	selstr = "i. "+ reses
	return selstr
def selElmTypeInRes(resnumlist, elmtypelist):
	elmtypelist = forceList(elmtypelist)
	emls = '+'.join([str(emltype) for emltype in elmtypelist])
	selstr = 'e. ' + emltype + ' and ' + elResNums(selResNums) + selResNums(selResNums)
	return selstr
def mkgreen(selstr):
	""""""
	cmd.color("green", selstr)
def mkred(selstr):
	""""""
	cmd.color("red", selstr)
def mkcolor(selstr, color):
	""""""
	cmd.color(color, selstr) 
def mkspheres(selstr):
	""""""
	cmd.show("spheres", selstr)
def mklines(selstr):
	""""""
	cmd.show("lines", selstr)
def slc(selstr, name):
	""""""
	cmd.select(name, selstr)
############################end samol###########################################
############################pymol_centrality_classes############################
import csv
import os
import operator
import warnings
def readcentralityCsv(fpath):
	with open( fpath, 'r') as f:
		reader = csv.reader(f)
		reader.next()
		l = []
		for row in reader:
			 l.append(ResCentrality(*row))
	return l
def savetocsv(headerrow, rows, fpath):
	fpath = os.path.abspath(fpath)
	with open(fpath, 'w') as f:
		csv_writer = csv.writer(f, dialect='excel')
		csv_writer.writerow(headerrow)
		for row in rows:
			csv_writer.writerow(row)
	return fpath
class ResCentrality(object):
	__slots__ = ['pymol', 'resnum', 'resname', 'region_name', 'ballesteros', 'centrality_score']
	def __init__(self, pymol, resnum, resname, region_name, ballesteros, centrality_score):
		self.pymol = str(pymol)
		self.resnum = int(resnum)
		self.resname = str(resname)
		self.region_name = str(region_name)
		self.ballesteros = str(ballesteros)
		self.centrality_score = float(centrality_score)
	def __repr__(self):
		attrvals = ', '.join(
			[repr(getattr(self, attrkey)) for attrkey in self.__slots__])
		reprstr =  self.__class__.__name__ + '('+ attrvals + ')'
		return reprstr
	@property
	def pymolg(self):
		return '////`%i/' % self.resnum
	def __sub__(self, other):
		if self.resnum != other.resnum:
			warnings.warn(
				'res %i from %s numbers not matchup with %i of %s' % (
					self.resnum, self.pymol, other.resnum, other.pymol
				)
			)
			return None
		resname = self.resname if self.resname == other.resname else ""
		region_name = self.region_name if self.region_name == other.region_name else ""
		ballesteros = self.ballesteros if self.ballesteros == other.ballesteros else ""
		centrality_score_dif = self.centrality_score - other.centrality_score
		return self.__class__(self.pymolg, self.resnum, resname, region_name, ballesteros, centrality_score_dif)

	def asList(self):
		"""returns a list of the sloted atribut values"""
		outList = []
		for slotattr in self.__slots__:
			outList.append(getattr(self, slotattr))
		return outList
class CentList(object):
	def __init__(self, *rescent):
		self.cents = rescent
		self.resnum_map = dict([(cent.resnum, idx)for idx, cent in enumerate(self.cents)])
	def __len__(self):
		return len(self.cents)
	def __iter__(self):
		return iter(self.cents)
	def __repr__(self):
		cents_as_str = ', '.join([repr(rescent) for rescent in self.cents])
		reprstr =  self.__class__.__name__ + '('+ cents_as_str + ')'
		return reprstr
	@classmethod
	def fromCsv(cls, csvfile):
		csvfile = os.path.abspath(csvfile)
		rows = []
		f = open(csvfile, 'r')
		reader = csv.reader(f)
		reader.next()
		for centdata in reader:
			rows.append(ResCentrality(*centdata))
		f.close()
		return cls(*rows)
	def _filter(self, callable_filter):
		outl = []
		for resCentrality in self.cents:
			if callable_filter(resCentrality):
				outl.append(resCentrality)
		return outl

	def __sub__(self, other_cent_list):
		"""other is a CentList will return a new CentList"""
		resCentralityDifs = []
		for rescent1 in self.cents:
			rescent2 = other_cent_list[rescent1]
			if not rescent2:
				warnings.warn(
					'res %i from %s numbers not other_cent_list no sub returned' % (
						rescent1.resnum, rescent1.pymol
					)
				)
				continue
			resCentralityDifs.append(rescent1-rescent2)
		return self.__class__(*resCentralityDifs)


	def __getitem__(self, resnum_or_rescentobj):
		if isinstance(resnum_or_rescentobj, ResCentrality):
			resnum_or_rescentobj = resnum_or_rescentobj.resnum
		resnum_index_in_cents = self._hasresnumandindex(resnum_or_rescentobj)
		if resnum_index_in_cents or resnum_index_in_cents == 0:
			return self.cents[resnum_index_in_cents]
		return None
		return self.cents[self.resnum_map[resnum_or_rescentobj]]
	
	def _hasresnumandindex(self, resnum):
		return self.resnum_map.get(resnum, None)

	def minCent(self):
		"""returns the rescent with the smallest centrality score"""
		minsofar, minrescent = self.cents[0].centrality_score, self.cents[0]
		for cent in self.cents:
			if cent.centrality_score < minsofar:
				minsofar = cent.centrality_score
				minrescent = cent
		return minrescent
	def maxCent(self):
		maxsofar, maxrescent = self.cents[0].centrality_score, self.cents[0]
		for cent in self.cents:
			if cent.centrality_score > maxsofar:
				maxsofar = cent.centrality_score
				maxrescent = cent
		return maxrescent

	def sortCent(self):
		sortedcents = sorted(self.cents, key=operator.attrgetter('centrality_score'))
		return self.__class__(*sortedcents)
	def getTop20percent(self):
		"""returns a list of resCents which are the top 20%"""
		index_of_top_20_percent = int(len(self.cents)*.2)
		sortedcents = self.sortCent()
		return sortedcents.cents[:index_of_top_20_percent]
	def get20percentCutOffNum(self):
		"""returns the centraliy value whihc is at the 20th% tile"""
		return self.getTop20percent()[-1].centrality_score
	def centBigerThen(self, cutoffnum=None):
		"""retursn list of resCents which have a centralit_score biggern then cutoffnum"""
		cutoffnum = cutoffnum if cutoffnum else self.get20percentCutOffNum()
		#defaults the cutoff biger then to the 20% value
		biggerthenList = []
		for rescent in self.cents:
			if rescent.centrality_score >= cutoffnum:
				biggerthenList.append(rescent)
		return biggerthenList

	def writeToCsv(self, fpath):
		"""writes CentList to excel csv
		take one param the path to the csv file to write the data to 

		TODO: KNOW ISSUE WITH SAVINE CSV AND THE ballesteros Trailing 0 cut off
			like 1.60 -> 1.6
		"""
		fpath = os.path.abspath(fpath)
		rows = [resCent.asList() for resCent in self.cents]
		headerrow = self.cents[0].__slots__
		savetocsv(headerrow, rows, fpath)
		return fpath
############################end pymol_centrality_classes########################
############################centrality_windows##################################
from Tkinter import *
def blankcb():
	""""""
	return 'hi'
def getRootTkWin():
	"""returns a new instance of a root TkWindow
	>>> root = getRootTkWin()
	"""
	return Tk()
class SingleScrollerWindow(Frame):
	valid_colors = ['yellow', 'red', 'green', 'brown', 'gold', 'gray', 'blue']
	scale_factor = 10000
	def __init__(self, parent, low, high, apply_cb=None):
		"""
		width=500, height=400
		"""
		if apply_cb == None:
			apply_cb = blankcb
		Frame.__init__(self, parent)
		if low < 1:
			low = low * self.scale_factor
		if high < 1:
			high = high * self.scale_factor
		self.Scl_low = low
		self.Scl_high = high
		self.Scl = Scale(self, from_=low, to=high, label='cut off', orient=HORIZONTAL)
		self.ClrLbl = Label(self, text="Color")
		self.ClrVar = StringVar()
		self.ClrInput = Entry(self, textvariable=self.ClrVar)
		self.Btn = Button(self, text='Apply', command=apply_cb)
		self.Txt = Text(self,height=10,width=70,background='white')
		self.Txt.tag_configure('bold_italics', font=('Verdana', 7, 'bold', 'italic'))

		self.Scl.grid(row=0,columnspan=2, sticky=W+E)
		self.ClrLbl.grid(row=1)
		self.ClrInput.grid(row=1, column=1)
		self.Btn.grid(row=3, columnspan=2, sticky=W+E)
		self.Txt.grid(row=4,column=0, columnspan=3)

		self.grid()

	def getDispText(self):
		return self.Txt.get('1.0', 'end')
	def appendText(self, text):
		self.Txt.insert(END, text)
	def setText(self, text):
		self.clearText()
		self.Txt.insert(END, text)
	def clearText(self):
		self.Txt.delete('1.0', 'end')

	def getScale(self):
		cur_sacle_val = float(self.Scl.get())
		if cur_sacle_val > 1:
			cur_sacle_val = cur_sacle_val / self.scale_factor
		return cur_sacle_val
	
	def setColor(self, color):
		self.ClrVar.set(color)

	def getColor(self):
		cur_color = self.ClrInput.get().strip()
		if cur_color not in self.valid_colors:
			cur_color = 'red'
			self.setColor(cur_color)
		return cur_color

	@property
	def VAL(self):
		return (self.getScale(), self.getColor())
############################end centrality_windows##############################
############################pymol_centrality_gui_manager########################
import tkFileDialog
class CentralityViewerManagerSimple():
	def __init__(self, **kwargs):
		self.startdir = kwargs.get('startdir', os.path.expanduser('~'))
		self.sourceCentralityCsv = self.askCsvName()
		self.centList = CentList.fromCsv(self.sourceCentralityCsv)
		self._rootwin = getRootTkWin()
		self.guiWin = SingleScrollerWindow(
			self._rootwin,
			self.centList.minCent().centrality_score,
			self.centList.maxCent().centrality_score,
			self.onApplyBtnCallback)

	@classmethod
	def initCallback(cls, *args, **kwargs):
		"""for when a intatination needed to be done in callback"""
		def guicallback():
			cls(*args, **kwargs)
		return guicallback


	def askCsvName(self, title="Select A Centrality Data CSV file"):
		startdir = self.startdir
		return tkFileDialog.askopenfilename(
			filetypes=[('.csv', '.csv')],
			initialdir=startdir,
			multiple=False,
			title=title
			)
	def getCentListFromCsv(self, fpath):
		"""returns a CentList instance """
		return CentList.fromCsv(fpath)

	def onApplyBtnCallback(self):
		# print(self.guiWin.VAL)
		# print(self.getCurrentHighlightedSet())
		# print(self.resNumWithCentralityBigerThenScale())
		self.updateDisplay()

	def getCurrentHighlightedSet(self):
		return set([int(resnum) for resnum in  self.guiWin.getDispText().strip().split(', ') if resnum])

	def resCentWithCentralityBigerThenScale(self):
		return self.centList.centBigerThen(self.guiWin.getScale())
	def resNumWithCentralityBigerThenScale(self):
		return set([rescent.resnum for rescent in self.resCentWithCentralityBigerThenScale()])

	def updateDisplay(self):
		highlited_res_nums = self.getCurrentHighlightedSet()
		updated_res_nums = self.resNumWithCentralityBigerThenScale()
		highlight_color = self.guiWin.getColor()
		removed_res_nums = highlited_res_nums - updated_res_nums
		added_res_nums = updated_res_nums - highlited_res_nums

		if removed_res_nums:
			removed_sel_str = selResNums(removed_res_nums)
			#resetColor(removed_sel_str)
			resetColorBugFix(removed_sel_str)
			resetShape(removed_sel_str)

		if added_res_nums:
			added_sel_str = selResNums(added_res_nums)#pymol sel str
			mkcolor(added_sel_str, highlight_color)
			mkspheres(added_sel_str)

		text_box_update = ', '.join([str(resnum) for resnum in updated_res_nums])

		self.guiWin.clearText()
		self.guiWin.setText(text_box_update)
		



############################end pymol_centrality_gui_manager####################
############################main################################################
def __pymolinit__(self=None):
	"""initilization function when run from within pymol"""
	startdir = os.environ.get("PYMOL_CENTRALITY_STARTDIR", os.path.expanduser('~'))
	self.menuBar.addmenuitem(
		'Plugin', 'command',  PLUGINNAME,
		label=PLUGINNAME, command=CentralityViewerManagerSimple.initCallback(startdir=startdir))
	#centralityViewerManagerSimple = CentralityViewerManagerSimple(startdir=startdir)
	# self.menuBar.addmenuitem(
	# 		'Plugin', 'command',  PLUGINNAME,
	# 		label=PLUGINNAME, command= lambda : centralityViewerManagerSimple._rootwin.mainloop())


	
def main(pymolself=None, startdir=None):
	if not startdir: startdir = os.path.expanduser('~')
	centralityViewerManagerSimple = CentralityViewerManagerSimple(startdir=startdir)
	centralityViewerManagerSimple._rootwin.mainloop()

############################end main############################################
############################@runtime############################################
# if INSIDE_PYMOL:
# 	#pymolgui.view_centrality = __name__
# 	def __init__(self):
# 		self.menuBar.addmenuitem(
# 			'Plugin', 'command',  PLUGINNAME,
# 			label=PLUGINNAME, command= lambda : main(self))
if __name__ == '__main__':
	TESTSTARTDIR = "/Users/sam/GoogleDrive/Academic/Masters/Research/CodeAndPrograms/GProInter/tests/datasets/CostanziEmail/Beta2_AR"
	main(startdir=TESTSTARTDIR)
############################end @runtime########################################


