#Dr. Costanzi email to Dr. Casey
Hi Steve,
A graduate student of mine, Sam Sheftel, and I are analyzing the network of non-covalent interactions among residues of the beta-2 adrenergic receptor.
Specifically, we are looking at the “network centrality” of each residue, which is a measure of how well connected a residue is with all the other residues, just like members if a social network.
Each residue will have a certain number of first-degree connections (i.e. directly connected residue), a certain number of second-degree connections (i.e. residues that are not directly connected but have a directly connected residue in common), and so on and so forth.
The centrality of a residue can be numerically expressed through a number that varies between 0 and 1, with 0 indicating a completely isolated residue, and 1 indicating a residue with first-degree connections with every other residue.
Centrality = (n -1) / (Summation of number of connections)
We analyzed a number of published crystal structures of the beta2 adrenergic receptor, solved in complex with a variety of ligands and in the inactive as well as the activated state and made some very interesting observations.
Observation 1: We first looked at the residues with the highest centrality in each structure. Choosing a cutoff for the centrality of 0.20, we found that all the residues with centrality >= 0.20 are located in the core of the receptor (see Excel sheet 1 and PowerPoint Slide 1).
Observation 2: Each structure has been crystallized with a different ligand. Most of these ligands are blockers (compounds that block the activity of the receptor). However, two of them are agonists (compounds that stimulate the activity of the receptor).  Thus, for each structure we looked at the difference in centrality when analyzing the network of interactions with or without the ligand. Choosing a cutoff of 0.015, we found that blockers and agonists increase the centrality of a different pattern of residues (see Excel sheet 2 and PowerPoint Slide 2).
Observation 3: All of the structures, with the exception of 1, have been crystallized in the inactive state. We calculated the difference in centrality between one inactive structure (2RH1) and all the other structures. Choosing cutoff values of 0.020 and -0.020, we found that while nothing changes when comparing inactive structures, the comparison between inactive and active structures shows that a number of residues located on the cytosolic side of the receptor loos centrality with the activation (see Excel sheet 3 and PowerPoint Slide 3). This is consistent with the conformational changes that occur during activation.
I am writing to you because, so far, we chose the cutoff values on the centrality or the difference in centrality arbitrarily. Although the cutoff values that we chose are great for what we want to show, I am wondering if there is a more elegant mathematical way to justify these cutoff values or to set more appropriate cutoff values.
If you are on campus, perhaps we can get together to discuss these issues.
Thank you for your help,
Stefano  

#---------------
#Responce

Hi Sam,

As you have probably seen from the email that I sent to Steve Casey, I came out with mathematically justified cutoff values.

For the centrality values, we will show all the all the data points higher than the average plus the standard deviation. For the difference in centrality values, we will show all the data points higher or lower than the average plus or minus 2.5 times the standard deviation.

In practice, the cutoff values are set as follows:
1) Centrality: 0.255
2) Centrality 3POG - Centrality other structures: +0.022 and - 0.026
3) Centrality with ligand - Centrality without ligand: +0.015 and -0.008 (note that there are no residues below -0.008, so we will only show the residues above 0.015)

I collected all the data in the attached spreadsheet and I made some nice plots for the paper/thesis (see attached powerpoint).

Fot the paper, I will make the figures with the Schrodinger suite. For the thesis, feel free to use the figures that I make or make your own with Pymol.

For the three analyses, I would like to have tables with the residues above and, when applicable, below the cutoff values (see attached template). Can you take care of these tables?

Thank you,